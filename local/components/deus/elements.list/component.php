<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Iblock;

if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 3600;
}

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if ($arParams["IBLOCK_ID"] < 1) {
    ShowError("IBLOCK_ID IS NOT DEFINED");
    return false;
}

if (!isset($arParams["ITEMS_LIMIT"])) {
    $arParams["ITEMS_LIMIT"] = 10;
}

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
	$arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));

$arNavParams = array();

if ($arParams["ITEMS_LIMIT"] > 0) {
    $arNavParams = array(
        "nPageSize" => $arParams["ITEMS_LIMIT"],
    );
}

if (!is_array($arParams["PROPERTY_CODE"]))
    $arParams["PROPERTY_CODE"] = array();
foreach ($arParams["PROPERTY_CODE"] as $key => $val)
    if ($val === "")
        unset($arParams["PROPERTY_CODE"][$key]);


$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["SET_BROWSER_TITLE"] = (isset($arParams["SET_BROWSER_TITLE"]) && $arParams["SET_BROWSER_TITLE"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_KEYWORDS"] = (isset($arParams["SET_META_KEYWORDS"]) && $arParams["SET_META_KEYWORDS"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_DESCRIPTION"] = (isset($arParams["SET_META_DESCRIPTION"]) && $arParams["SET_META_DESCRIPTION"] === 'N' ? 'N' : 'Y');

$arNavigation = CDBResult::GetNavParams($arNavParams);

if ($this->StartResultCache(false, array($arNavigation,$arrFilter))) {

    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError("IBLOCK_MODULE_NOT_INSTALLED");
        return false;
    }

    $bGetProperty = count($arParams["PROPERTY_CODE"]) > 0;

    $arSort = array("SORT" => "ASC", "DATE_ACTIVE_FROM" => "DESC", "ID" => "DESC");

    if($arParams['SORT_FIELD']){
        $arSort = array($arParams['SORT_FIELD']=>$arParams['SORT_ORDER']);
    }


    $bSectionFound = false;
    //Hidden triky parameter USED to display linked
    //by default it is not set
    if($arParams["BY_LINK"]==="Y")
    {
        $arResult = array(
            "ID" => 0,
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        );
        $bSectionFound = true;
    }
    elseif($arParams["SECTION_ID"] > 0)
    {
        $arFilter["ID"]=$arParams["SECTION_ID"];
        $rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
        $rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
        $arResult = $rsSection->GetNext();
        if($arResult)
            $bSectionFound = true;
    }
    elseif(strlen($arParams["SECTION_CODE"]) > 0)
    {
        $arFilter["=CODE"]=$arParams["SECTION_CODE"];
        $rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
        $rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
        $arResult = $rsSection->GetNext();
        if($arResult)
            $bSectionFound = true;
    }
    elseif(strlen($arParams["SECTION_CODE_PATH"]) > 0)
    {
        $sectionId = CIBlockFindTools::GetSectionIDByCodePath($arParams["IBLOCK_ID"], $arParams["SECTION_CODE_PATH"]);
        if ($sectionId)
        {
            $arFilter["ID"]=$sectionId;
            $rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
            $rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
            $arResult = $rsSection->GetNext();
            if($arResult)
                $bSectionFound = true;
        }
    }
    else
    {
        //Root section (no section filter)
        $arResult = array(
            "ID" => 0,
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        );
        $bSectionFound = true;
    }
    if(!$bSectionFound)
    {
        $this->abortResultCache();
        Iblock\Component\Tools::process404(
            trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_SECTION_NOT_FOUND")
            ,true
            ,$arParams["SET_STATUS_404"] === "Y"
            ,$arParams["SHOW_404"] === "Y"
            ,$arParams["FILE_404"]
        );
        return;
    }
    elseif($arResult["ID"] > 0 && $arParams["ADD_SECTIONS_CHAIN"])
    {
        $arResult["PATH"] = array();
        $rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arResult["ID"]);
        $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
        while($arPath = $rsPath->GetNext())
        {
            $ipropValues = new Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
            $arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
            $arResult["PATH"][]=$arPath;
        }
    }

    if ($arResult["ID"] > 0)
    {
        $ipropValues = new Iblock\InheritedProperty\SectionValues($arResult["IBLOCK_ID"], $arResult["ID"]);
        $arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
    }
    else
    {
        $arResult["IPROPERTY_VALUES"] = array();
    }

    Iblock\Component\Tools::getFieldImageData(
        $arResult,
        array('PICTURE', 'DETAIL_PICTURE'),
        Iblock\Component\Tools::IPROPERTY_ENTITY_SECTION,
        'IPROPERTY_VALUES'
    );

    $arFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ACTIVE_DATE" => "Y");
    $arSelect = array_merge($arParams["FIELD_CODE"], array(
        "ID",
        "IBLOCK_ID",
        "ACTIVE_FROM",
        "NAME",
        "DETAIL_PAGE_URL",
    ));

    $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, $arSelect);

    if ($arParams["DETAIL_URL"]) {
        $rsElement->SetUrlTemplates($arParams["DETAIL_URL"]);
    }

    while ($obElement = $rsElement->GetNextElement()) {

        $arElement = $obElement->GetFields();
        if ($arElement["PREVIEW_PICTURE"]) {
            $arElement["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);

			$arPhotoSmall = CFile::ResizeImageGet(
				$arElement["PREVIEW_PICTURE"]["ID"],
				array(
					'width'=> intval($arParams['IMG_WIDTH']),
					'height'=>intval($arParams['IMG_HEIGHT'])
				),
				BX_RESIZE_IMAGE_EXACT,
				Array(
					"name" => "sharpen",
					"precision" => 0
				)
			);
            $arElement["RESIZED_PICTURE"] = array(
				'SRC' => $arPhotoSmall['src']
			);
        }

		if(strlen($arElement["ACTIVE_FROM"])>0)
			$arElement["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arElement["ACTIVE_FROM"], CSite::GetDateFormat()));
		else
			$arElement["DISPLAY_ACTIVE_FROM"] = "";

        $arButtons = CIBlock::GetPanelButtons(
            $arElement["IBLOCK_ID"],
            $arElement["ID"],
            0,
            array("SECTION_BUTTONS"=>false, "SESSID"=>false)
        );
        $arElement["ADD_LINK"] = $arButtons["edit"]["add_element"]["ACTION_URL"];
        $arElement["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
        $arElement["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

        $ipropValues = new Iblock\InheritedProperty\ElementValues($arElement["IBLOCK_ID"], $arElement["ID"]);
        $arElement["IPROPERTY_VALUES"] = $ipropValues->getValues();

        Iblock\Component\Tools::getFieldImageData(
            $arElement,
            array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
            Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
            'IPROPERTY_VALUES'
        );

        if ($bGetProperty)
            $arElement["PROPERTIES"] = $obElement->GetProperties();

        $arElement["DISPLAY_PROPERTIES"] = array();
        foreach ($arParams["PROPERTY_CODE"] as $pid) {
            $prop = &$arElement["PROPERTIES"][$pid];
            if (
                (is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0)
                || (!is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0)
            ) {
                $arElement["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arElement, $prop, "news_out");
            }
        }

        $arResult["ITEMS"][] = $arElement;
    }
    $arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, "", "", "");

    $this->SetResultCacheKeys(array(
        "ID",
        "IBLOCK_TYPE_ID",
        "LIST_PAGE_URL",
        "NAV_CACHED_DATA",
        "NAME",
        "SECTION",
        "ELEMENTS",
        "IPROPERTY_VALUES",
        "ITEMS_TIMESTAMP_X",
    ));
    $this->IncludeComponentTemplate();
}


if($arParams["SET_TITLE"])
{
    if ($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
        $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
    elseif(isset($arResult["NAME"]))
        $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
}

if ($arParams["SET_BROWSER_TITLE"] === 'Y')
{
    $browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
        $arResult, $arParams["BROWSER_TITLE"]
        ,$arResult["IPROPERTY_VALUES"], "SECTION_META_TITLE"
    );
    if (is_array($browserTitle))
        $APPLICATION->SetPageProperty("title", implode(" ", $browserTitle), $arTitleOptions);
    elseif ($browserTitle != "")
        $APPLICATION->SetPageProperty("title", $browserTitle, $arTitleOptions);
}

if ($arParams["SET_META_KEYWORDS"] === 'Y')
{
    $metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
        $arResult, $arParams["META_KEYWORDS"]
        ,$arResult["IPROPERTY_VALUES"], "SECTION_META_KEYWORDS"
    );
    if (is_array($metaKeywords))
        $APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
    elseif ($metaKeywords != "")
        $APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);
}

if ($arParams["SET_META_DESCRIPTION"] === 'Y')
{
    $metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
        $arResult, $arParams["META_DESCRIPTION"]
        ,$arResult["IPROPERTY_VALUES"], "SECTION_META_DESCRIPTION"
    );
    if (is_array($metaDescription))
        $APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
    elseif ($metaDescription != "")
        $APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);
}
?>