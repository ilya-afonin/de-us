<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch()) {
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];
}

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
	{
		$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}

$arComponentParameters = array(
	"GROUPS" => array(
		"IMG_BLOCK" => array(
			"NAME" => Loc::getMessage('IMG_BLOCK'),
			"SORT" => 100,
		)
	),
	"PARAMETERS" => array(

		"COLOR" => Array(
			"PARENT" => "BASE",
			"NAME" => 'Выбор цвета',
			"TYPE" => "COLORPICKER",
			"DEFAULT" => 'FFFF00'
		),
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('IBLOCK_TYPE'),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('IBLOCK_ID'),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '={$_REQUEST["ID"]}',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"ITEMS_LIMIT" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('ITEMS_LIMIT'),
			"TYPE" => "STRING",
			"DEFAULT" => "10",
		),
		"IMG_WIDTH" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('IMG_WIDTH'),
			"TYPE" => "STRING",
			"DEFAULT" => "256",
		),
		"IMG_HEIGHT" => Array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('IMG_HEIGHT'),
			"TYPE" => "STRING",
			"DEFAULT" => "256",
		),
		"ACTIVE_DATE_FORMAT" => CIBlockParameters::GetDateFormat("Формат даты", "ADDITIONAL_SETTINGS"),
		"FIELD_CODE" => CIBlockParameters::GetFieldCode(Loc::getMessage('IBLOCK_FIELD'), "DATA_SOURCE"),
		"PROPERTY_CODE" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => Loc::getMessage('T_IBLOCK_PROPERTY'),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arProperty_LNS,
			"ADDITIONAL_VALUES" => "Y",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),

	),
);
?>