<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Выборка элементов инфоблока",
	"DESCRIPTION" => "Облегченный компонент выборки элементов инфоблока",
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "Cakelabs",
	),
	"COMPLEX" => "N",
);

?>