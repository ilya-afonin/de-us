<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS = array(
    'IBLOCK_TYPE' => 'Тип инфоблока',
    'IBLOCK_ID' => 'ID инфоблока',
    'ITEMS_LIMIT' => 'Количество элементов',
    'IMG_BLOCK' => 'Изображения',
    'IMG_WIDTH' => 'Ширина миниатюры',
    'IMG_HEIGHT' => 'Высота миниатюры',
    'IBLOCK_FIELD' => 'Поля инфоблока',
    'T_IBLOCK_PROPERTY' => 'Свойства инфоблока',
);