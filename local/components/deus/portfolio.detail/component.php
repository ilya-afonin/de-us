<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}

if (!isset($arParams["CACHE_TIME"])) {
  $arParams["CACHE_TIME"] = 3600;
}

$arParams['PATH_TO_PORTFOLIO'] = !empty($arParams['PATH_TO_PORTFOLIO']) ? $arParams['PATH_TO_PORTFOLIO'] : "/portfolio/";

if ($this->StartResultCache(false, array($arNavigation))) {

  CModule::IncludeModule("iblock");

  $arFilter = Array(
    "IBLOCK_ID" => 1,
    "ACTIVE" => "Y",
    "CODE" => $arParams["ELEMENT_CODE"]
  );

  $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false);

  if ($ob = $res->GetNextElement()) {
    $arResult = $ob->GetFields();
    $arResult["PROPERTIES"] = $ob->GetProperties();
  }


  $arFilter = Array(
    "IBLOCK_ID" => 1,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
  );

  if(!$USER->IsAdmin())
    $arFilter[] = array("PROPERTY_IS_SHOW_ADMIN" => false);

  $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, Array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'PROPERTY_NEXT_PICTURE', 'PROPERTY_WHITE_FOOTER'), false);

  while ($ob = $res->GetNext()) {
    $objs[] = array(
      "ID" => $ob["ID"],
      "DETAIL_PAGE_URL" => $ob["DETAIL_PAGE_URL"],
      "NAME" => $ob["NAME"],
      "DETAIL_PICTURE" => ($ob['PROPERTY_NEXT_PICTURE_VALUE'])?$ob['PROPERTY_NEXT_PICTURE_VALUE']:$ob["DETAIL_PICTURE"],
      "WHITE_FOOTER" => ($ob['PROPERTY_WHITE_FOOTER_VALUE'] == 'Y')?true:false,
    );
  }

  for ($i = 0; $i < count($objs); $i++) {
    if ($objs[$i]["ID"] == $arResult["ID"]) {
      $next = $i + 1;
      $prev = $i - 1;

      if ($prev < 0) {
        $prev = count($objs) - 1;
      }

      if ($next == count($objs)) {
        $next = 0;
      }
    }
  }

  $arResult["NEXT"]['URL'] = $objs[$next]["DETAIL_PAGE_URL"];
  $arResult["NEXT"]['IMG'] = $objs[$next]["DETAIL_PICTURE"];
  $arResult["NEXT"]['NAME'] = $objs[$next]["NAME"];
  $arResult["NEXT"]['WHITE_FOOTER'] = $objs[$next]["WHITE_FOOTER"];

  $arResult["PREV"]['URL'] = $objs[$prev]["DETAIL_PAGE_URL"];
  $arResult["PREV"]['IMG'] = $objs[$prev]["DETAIL_PICTURE"];
  $arResult["PREV"]['NAME'] = $objs[$prev]["NAME"];


  $arResult["SECTION"]["SECTION_PAGE_URL"] = "/works/";

  $this->SetResultCacheKeys(array(
    "ID",
    "IBLOCK_ID",
    "CODE",
    "NAV_CACHED_DATA",
    "NAME",
    "IBLOCK_SECTION_ID",
    "IBLOCK",
    "LIST_PAGE_URL",
    "~LIST_PAGE_URL",
    "SECTION",
    "PROPERTIES",
    "NEXT",
    "PREV",
    "PREVIEW_TEXT"
  ));

  $this->IncludeComponentTemplate();

  if (empty($arResult)) {
    $this->AbortResultCache();
    ShowError("404 Not Found");
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
  }

}
//echo "<pre style='display:none'>";print_r($arResult);echo "</pre>";
$APPLICATION->SetTitle($arResult["NAME"]);
$APPLICATION->SetPageProperty('description', strip_tags($arResult["PREVIEW_TEXT"]));

if($arResult['PROPERTIES']["WHITE_HEADER"]['VALUE'] == 'Y')
    $APPLICATION->SetPageProperty('portfolioBlack', 'header--black');
if($arResult['PROPERTIES']['WHITE_MENU']['VALUE'] == 'Y')
    $APPLICATION->SetPageProperty('menuWhite', 'is-white');
if($arResult["NEXT"]['WHITE_FOOTER'])
    $APPLICATION->SetPageProperty('footerWhite', 'footer--white');

//ПОДГРУЖАЕМ ВЕРСТКУ ИЗ ВНЕШНИХ ФАЙЛОВ
$dir = $_SERVER["DOCUMENT_ROOT"].$arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/js";   //задаём имя директории

if (is_dir($dir)) {
  $files = scandir($dir);
  array_shift($files);
  array_shift($files);

  for ($i = 0; $i < sizeof($files); $i++) {
    if (substr_count($files[$i], ".js") > 0) {
      $APPLICATION->AddHeadScript($arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/js/".$files[$i]);
    }
  }
}

if (file_exists($_SERVER["DOCUMENT_ROOT"].$arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/script.js")) {
  $APPLICATION->AddHeadScript($arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/script.js");
}

$dir = $_SERVER["DOCUMENT_ROOT"].$arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/css";   //задаём имя директории

if (is_dir($dir)) {
  $files = scandir($dir);
  array_shift($files);
  array_shift($files);

  for ($i = 0; $i < sizeof($files); $i++) {
    if (substr_count($files[$i], ".css") > 0) {
      $APPLICATION->SetAdditionalCSS($arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/css/".$files[$i]);
    }
  }
}

if (file_exists($_SERVER["DOCUMENT_ROOT"].$arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/styles.css")) {
  $APPLICATION->SetAdditionalCSS($arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/styles.css");
}
if (file_exists($_SERVER["DOCUMENT_ROOT"].$arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/media.css")) {
  $APPLICATION->SetAdditionalCSS($arParams['PATH_TO_PORTFOLIO'].$arResult["CODE"]."/media.css");
}
/*
$APPLICATION->IncludeFile(
		"/tpl/include_areas/project_arrows.php",
		Array("barrows_x"=>$arResult["PROPERTIES"]["barrows_x"]["VALUE"],  "barrows_y"=>$arResult["PROPERTIES"]["barrows_y"]["VALUE"], "barrows_color"=>$arResult["PROPERTIES"]["barrows_color"]["VALUE"],"tarrows_x"=>$arResult["PROPERTIES"]["tarrows_x"]["VALUE"],  "tarrows_y"=>$arResult["PROPERTIES"]["tarrows_y"]["VALUE"], "tarrows_color"=>$arResult["PROPERTIES"]["tarrows_color"]["VALUE"], "NEXT_URL"=>$arResult["NEXT_URL"], "PREV_URL"=>$arResult["PREV_URL"]),
		Array("MODE"=>"php")
);

$APPLICATION->IncludeFile(
		"/tpl/include_areas/project_price.php",
		Array("price"=>$arResult["PROPERTIES"]["price"]["VALUE"], "price_x"=>$arResult["PROPERTIES"]["price_x"]["VALUE"], "price_y"=>$arResult["PROPERTIES"]["price_y"]["VALUE"], "price_color"=>$arResult["PROPERTIES"]["price_color"]["VALUE"]),
		Array("MODE"=>"php")
);
*/
?>