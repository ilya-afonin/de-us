<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<style>
    .accent_bg {
        background-color: <?=$arResult['PROPERTIES']['COLOR']['VALUE']?> !important;
    }
    .accent_border {
        border-bottom: 2px solid <?=$arResult['PROPERTIES']['COLOR']['VALUE']?> !important;
    }
</style>

<? if(!empty($arResult['DETAIL_PICTURE'])) {
  $bg_img = 'background-image: url('.CFile::GetPath($arResult['DETAIL_PICTURE']).');';
}
if($arResult['PROPERTIES']['BG_TRANSPARENT']['VALUE'] == 'Y') {
  $bg_color = 'background-color: transparent;';
  $main_color = 'color: #fff !important;';
}
elseif(!empty($arResult['PROPERTIES']['BG_COLOR']['VALUE'])) {
    $bg_color = 'background-color: '.$arResult['PROPERTIES']['BG_COLOR']['VALUE'].';';
}
?>
<div class="case">
    <div class="case__welcome" style="<?=$bg_img.$bg_color?>">
        <div class="container">
            <div class="case__welcome-wrapper">
                <div class="case__welcome-content wow fadeIn" data-wow-delay="1000ms">
                    <div class="case__welcome-title wow fadeInLeftBig"
                         data-wow-delay="100ms"><?= html_entity_decode($arResult['PROPERTIES']['DETAIL_NAME']['VALUE']['TEXT']); ?>
                    </div>
                    <div class="case__welcome-arrow accent_bg wow fadeInLeftBig" data-wow-delay="200ms"></div>
                    <div class="case__welcome-text wow fadeInLeftBig" data-wow-delay="300ms"><?= $arResult['DETAIL_TEXT']; ?>
                    </div>
                </div>

                <div class="case__welcome-bg">
                    <img class="case__welcome-img wow fadeIn" data-wow-delay="400ms"
                         src="<?= CFile::GetPath($arResult['PROPERTIES']['DETAIL_IMG']['VALUE']) ?>" alt="<?= $arResult['NAME'] ?>">
                </div>
            </div>
        </div>
    </div>

    <div class="case__wrapper">
        <div class="container">
            <div class="case__main" style="<?=$main_color?>">
                <div class="case__main-task wow fadeIn" data-wow-delay="100ms" data-wow-offset="200">
                    <div class="case__main-task-title accent_border">Задача</div>
                    <div class="case__main-task-desc" style="<?=$main_color?>">
                        <?= $arResult['PROPERTIES']['TASK']['VALUE'] ?>
                    </div>
                </div>
                <div class="case__main-type-client wow fadeIn" data-wow-offset="200" data-wow-delay="100ms">
                    <div class="case__main-type">
                        <div class="case__main-type-title">Тип проекта</div>
                        <div class="case__main-type-desc"><?= $arResult['PROPERTIES']['TYPE']['VALUE'] ?></div>
                    </div>
                    <div class="case__main-client">
                        <div class="case__main-client-title">Клиент</div>
                        <div class="case__main-client-desc"><?= $arResult['PROPERTIES']['CLIENT']['VALUE'] ?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="case__container">
            <? $APPLICATION->IncludeFile(
                $arParams['PATH_TO_PORTFOLIO'] . $arResult["CODE"] . "/index.php",
                Array(
                    "arResult" => $arResult,
                    "NEXT_URL" => $NEXT_URL,
                    "PREV_URL" => $PREV_URL
                ),
                Array("MODE" => "php")
            );
            ?>
        </div>

        <div class="case__next-wrapper<?=($arResult['NEXT']['WHITE_FOOTER'])?' case__next-wrapper--white':''?>">
            <a class="case__next" href=" <?= $arResult['NEXT']['URL']; ?>" style="background-image: url('<?=CFile::GetPath($arResult['NEXT']['IMG']);?>')">
                <?= $arResult['NEXT']['NAME']; ?>
            </a>
            <div class="case__next-title">Следующий проект</div>
        </div>
    </div>
</div>