'use strict';

$(document).ready(function () {

    headerScroll();
    headerMobile();
    cSlider();
    formValidate();
    focusInputs();
    dropzone();
    scrollBar();
    smoothScroll();
    tagsAjax();
    popup();
    hoverCase();
    caseSlider();
});

function caseSlider() {

    if ($('.case__slider').length > 0) {

        var slider = $('.case__slider');

        slider.owlCarousel({
            items: 1,
            loop: true,
            //animateIn: 'fadeIn',
            //animateOut: 'fadeOut',
            nav: true,
            dots: false,
            //navContainer: navigation,
            navText: '',
            navElement: 'a',
            navContainerClass: 'case-arrows',
            navClass: ['case-arrows__arrow case-arrows__arrow--prev', 'case-arrows__arrow case-arrows__arrow--next']
        });
    }
}

function hoverCase() {
    if ($('.footer--white').length > 0) {
        $('body').on('mouseenter', '.case__next', function () {
            $('.footer--white').addClass('hover');
        });
        $('body').on('mouseleave', '.case__next', function () {
            $('.footer--white').removeClass('hover');
        });
    }
}
function popup() {

    $('body').on('click', '[data-modal]', function () {
        var target = $(this).attr('data-modal');

        var src = $(target).find('iframe').attr('data-src');
        if (src) {
            $(target).find('iframe').attr("src", src);
        }
        $(target).addClass('is-active');
        $("body").css('overflow', 'hidden');
        return false;
    });

    $('body').on('click', '.popup__closer', function () {
        popupClose();
    });

    $('body').on('click', '.popup__overlay', function () {
        popupClose();
    });
}

function popupClose() {
    $('.popup').removeClass('is-active');
    $('.popup').fadeOut(400);
    $('body').css('overflow', 'visible');
}

function cSlider() {

    if ($('.clients-slider').length > 0) {

        var slider = $('.c-slider__scene');

        slider.each(function () {

            $(this).owlCarousel({
                items: 1,
                loop: true,
                nav: false,
                dots: true,
                /*nestedItemSelector: 'c-slider__slide',*/
                responsive: {
                    0: {
                        mouseDrag: true,
                        center: true
                    },
                    1024: {
                        mouseDrag: false
                    },
                    1200: {
                        center: false
                    }
                }
            });
        });
    }
}

function headerMobile() {

    $('body').on('click', '.header__burger', function () {

        $(this).toggleClass('is-active');
        $('.header__row').toggleClass('is-opened');
        $('body').toggleClass('is-unscrolled');
    });

    $('body').on('click', '.header__nav-link', function () {
        if ($('.header__row').hasClass('is-opened')) $('.header__row').removeClass('is-opened');
        if ($('body').hasClass('is-unscrolled')) $('body').removeClass('is-unscrolled');
    });
}

function headerScroll() {

    if ($('.header').length > 0) {

        var currentPosition = $(window).scrollTop();
        var lastPosition = 0;
        var header = $('.header');

        var condition = true;

        if (currentPosition > 0) {

            if (!header.hasClass('is-colored')) header.addClass('is-colored');
        }

        /*$(window).on('resize', function () {
             if ($(window).width() < 1009) {
                 header.removeClass('is-hidden');
                header.removeClass('is-colored');
            }
        });
        */
        $(window).on('scroll', function () {

            currentPosition = $(window).scrollTop();

            if (currentPosition < 0) {

                return false;
            } else if (currentPosition > lastPosition && condition) {

                header.addClass('is-hidden');

                condition = false;
            } else if (currentPosition < lastPosition && (currentPosition == 0 || currentPosition === lastPosition)) {

                header.removeClass('is-hidden');
                header.removeClass('is-colored');

                condition = true;
            } else if (currentPosition < lastPosition && currentPosition > 0 && !condition) {

                header.removeClass('is-hidden');
                header.addClass('is-colored');

                condition = true;
            }

            lastPosition = currentPosition;
        });

        $(window).on('scroll', function () {

            var scrolled = $(window).scrollTop();

            if (scrolled > 65) {

                $('.header').addClass('header--custom');
            } else {

                $('.header').removeClass('header--custom');
            }
        });
    }
}

function focusInputs() {

    (function () {

        $('input.c-form__input-field').each(function () {
            // in case the input is already filled..
            if ($.trim($(this).val()) !== '') {
                $(this).parent().addClass('is-full');
            }

            // events:
            $(this).on('focus', function () {
                // console.log($(this));
                $(this).closest('.c-form__input').addClass('is-full');

                //ev.target.parentNode.classList.add('input--filled');
            });

            $(this).on('blur', function () {

                if ($.trim($(this).val()) === '') {
                    $(this).closest('.c-form__input').removeClass('is-full');
                    //ev.target.parentNode.classList.remove('input--filled');
                }
            });
        });
    })();
}

function formValidate() {

    if ($('.validate').length > 0) {

        $('.validate').each(function () {
            var block = $('.c-form__input-field');

            $(this).validate({

                focusInvalid: false,
                highlight: function highlight(element, errorClass) {
                    $(element).closest('.c-form__input').find('.c-form__input-label').addClass('c-form__input-label--error');
                },
                unhighlight: function unhighlight(element, errorClass) {
                    $(element).closest('.c-form__input').find('.c-form__input-label').removeClass('c-form__input-label--error');
                },

                rules: {

                    name: {
                        required: true
                    },

                    email: {
                        required: true,
                        email: true
                    },

                    phone: {
                        required: true
                    }
                },

                messages: {
                    name: '',
                    email: '',
                    phone: '',
                    privacy: ''
                },
                submitHandler: function submitHandler(form) {
                    /*console.log($('.popup__privacy-cb').attr('checked'));
                     if ($('.popup__privacy-cb').attr('checked') == undefined) {
                        return false;
                    }*/

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: '/local/tools/form-handler.php',
                        data: $(form).serialize(),
                        error: function error() {
                            console.log("Error messages");
                        },
                        success: function success(data) {
                            if (!data.error) {

                                $(form).find('input[type="text"]').value = '';
                                $(form).find('.c-form__error').fadeOut();

                                $(form).find(".dz-message .dz-remove").click();

                                $('#pop-result').fadeIn(400);
                                $('#pop-result').addClass('is-active');

                                $('input.c-form__input-field').each(function () {
                                    $(this).blur();
                                });

                                //$(form).parents('.popup').addClass("is-submitted");
                                /*if (!!$(form).data("goal"))
                                    yaCounter6697801.reachGoal($(form).data("goal"));*/
                            } else {
                                $(form).find('.c-form__error').text(data.message);
                                $(form).find('.c-form__error').fadeIn();
                            }
                        }
                    });
                }
            });

            $(".mask--phone").mask("+7 (999) 999-99-99");
        });
    }
}

function dropzone() {

    if ($('.dZUpload').length > 0) {

        $('.dZUpload').dropzone({
            /*url : "/file-upload",
             addRemoveLinks: true, //add remove link
             dictRemoveFile: 'Удалить файл', //remove link text
             dictCancelUpload: 'Отменить загрузку',
             init: function () {
                this.on("addedfile", function() {
                 if (this.files[1]!=null){
                     this.removeFile(this.files[0]);
                    }
                });
                 this.on("complete", function (file) {
                  if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    $('.dz-preview').prepend('<p class="complete"> Файл успешно загружен </p>');
                  }
                });
            }*/
            url: "/local/tools/upload_main.php",

            addRemoveLinks: true, //add remove link

            dictRemoveFile: '+', //remove link text

            dictCancelUpload: 'Cancel upload',

            dictMaxFilesExceeded: 'Вы не можете загружать больше файлов. Максимальное количество файлов: {{maxFiles}}',
            dictDefaultMessage: "Перетащите сюда файлы для загрузки",
            dictFallbackMessage: "Ваш браузер не поддерживает перетаскивание файлов.",
            dictFallbackText: "Пожалуйста, используйте резервную форму ниже, чтобы загрузить файлы, как в старые времена.",
            dictFileTooBig: "Файл слишком большой ({{filesize}}MiB). Максимальный размер файла: {{maxFilesize}}MiB.",
            dictInvalidFileType: "Вы не можете загружать файлы подобного типа.",
            dictResponseError: "Сервер ответил кодом {{statusCode}}.",
            dictCancelUploadConfirmation: "Вы уверены, что хотите отменить эту загрузку?",

            maxFiles: 1,
            maxFilesize: 20,
            init: function init() {
                var myDropZone = this;
                var $message = $('.dz-message');
                var uploadFilesField = $('input[name="uploaded_files"]');

                this.on("addedfile", function (e) {
                    // if (this.files[1]!=null){
                    //     this.removeFile(this.files[0]);
                    // }
                }), this.on("success", function (file, response) {
                    if (response == 1) {
                        $message.find('span').text(file.name);
                        $message.append(file._removeLink);
                        $message.closest('.c-form__input').addClass('is-full');
                        $('.dz-icon').css('display', 'none');

                        $('.dz-preview').prepend('<p class="complete"> File successfully uploaded</p>');

                        uploadFilesField.val(file['name']);
                    } else {
                        $('.dz-preview').prepend('<p class="complete error">Error loading file!</p>');
                    }

                    $(".dz-remove").click(function (e) {
                        e.preventDefault();
                        //$('.dz-preview').remove();
                        $message.find('.dz-remove').remove();
                        $('.dz-icon').css('display', 'block');
                        $message.find('span').text('Выберите файл');
                        $message.closest('.c-form__input').removeClass('is-full');
                        uploadFilesField.value = '';
                        myDropZone.removeAllFiles();
                    });
                }), this.on("error", function (file, message) {
                    $(file.previewElement).prepend('<p class="complete error">Error loading file! ' + message + '</p>');
                });
                // this.on("complete", function (file) {
                //     console.log(this.getUploadingFiles().length,'this.getUploadingFiles().length');
                //     if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                //
                //     }
                // });
            }
        });
    }
}

function scrollBar() {
    if ($('.works__sections').length > 0 && $(window).width() < 767) {
        $(".works__sections").mCustomScrollbar({
            axis: "x",
            theme: "dark-3"
        });
    }
}

function smoothScroll() {

    if (window.location.hash) scroll(0, 0);
    setTimeout(function () {
        scroll(0, 0);
    }, 1);

    $(function () {
        $('.scroll').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: $($(this).attr('href')).offset().top + 'px'
            }, 1000, 'swing');
        });

        if (window.location.hash) {
            // smooth scroll to the anchor id
            $('html,body').animate({
                scrollTop: $(window.location.hash).offset().top - 80 + 'px'
            }, 1000, 'swing');
        }
    });

    /*var scroll = new SmoothScroll('a[href*="#"]', {
         // Selectors
        ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
        header: $('.header'), // Selector for fixed headers (must be a valid CSS selector)
        topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"
         // Speed & Easing
        speed: 700, // Integer. How fast to complete the scroll in milliseconds
        //clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
        offset: 2000,
        easing: 'easeInOutCubic', // Easing pattern to use
        customEasing: function (time) {
             // Function. Custom easing pattern
            // If this is set to anything other than null, will override the easing option above
             // return <your formulate with time as a multiplier>
             // Example: easeInOut Quad
            return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;
         },
         // History
        updateURL: true, // Update the URL on scroll
        popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)
         // Custom Events
        emitEvents: true // Emit custom events
     });*/
}

function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}

function tagsAjax() {

    //var itemNubmer;
    //var defaultItem = 1;
    //define  here default visible tab and menu item
    //var tab = container.find($('.tabs__content'));
    //var defaultController = $('.tabs .tabs__ul .tabs__li:nth-child(' + defaultItem + ')');

    //defaultController.addClass('active');
    //sliders fix helper
    /*tab.fadeOut(0);
    $('.tabs__content:nth-child(' + defaultItem + ')').fadeIn();
    $('.tabs__content:nth-child(' + defaultItem + ')').addClass('tabs__content--active');*/

    $('body').on('click', '.works__section, .works__tag', function (e) {

        e.preventDefault();

        var container = $('.works__list');

        //var tab = $(this).parent().next().children('.tabs__content');
        //var tabs_container = $(this).parent().next();
        //links activate\

        var $link = $(e.target);

        var $another_links = $link.siblings();
        if ($another_links.hasClass('active')) $another_links.removeClass('active');
        $link.addClass('active');

        //toggle tabs
        var href = $link.attr("href");
        var tag = gup('tag', href);
        var t;
        if (tag !== null) t = '?tag=' + tag;else t = '';

        if (href !== "" && href !== "#") {
            //$(tabs_container).find('.tabs__ajax__all').remove();
            $(container).empty();
            //$(tabs_container).find('.tabs__content').before('<div class="tabs__ajax__all"></div>');

            history.pushState({}, '', href);
            $.ajax({
                url: '/local/tools/ajax/portfolio_list.php' + t,
                dataType: "html",
                data: { ajax: 'y' },
                error: function error() {
                    console.log("Error send message");
                },
                success: function success(data) {
                    $(container).html(data);
                    App.updateController();
                    $(container).fadeIn(300, function () {
                        $('.works__item').addClass('visible');
                    });
                }
            });
            return false;
        }
        return false;
    });
}

function is_mobile() {
    var e = new MobileDetect(window.navigator.userAgent);
    return e.mobile() || e.tablet();
};

var AppClass = function () {
    var o = void 0;

    function e() {
        this.$doc = $(document), this.$win = $(window), this.$body = $("body"), this.$navMenu = $(".header__links"), this.$navMenuContent = $(".header__nav"), this.$footer = $(".footer"), this.$caseMenu, this.caseMenuPosition, this.isMobile = is_mobile(), this.$page = $(".page"), this.$promo = $(".promo"), this.$case = $(".case"), this.$logo = $(".animated-logo"), this.$title = $(".animated-title"), this.$promoButton = $(".promo__button"), this.$caseContainer = $(".case__wrapper"), this.$contentContainer = $(".content"), this.tempResponse, this.winHeight = $(window).height(), this.scrollbar, this.controller, this.contentHeight = $(".page").height(), this.scrollHeight = 0, this.animating = !1, this.$animatedVideo, this.$animatedTitle, this.$menuLinks = $(".menu-hover"), this.$menuImages = $(".menu-image"),
        //$(".svg__template").length && (this.svgTemplate = Handlebars.compile($(".svg__template").html())),
        this.fixedElements = [], (o = this).init();
    }

    return o = [], e.prototype.init = function () {
        //o.initMenuIcon(),
        //o.initLogo(),
        //o.initAjaxLinks(),
        //o.initBackButton();
        var nav = navigator.userAgent.toLowerCase();
        -1 != nav.indexOf("safari") && (-1 < nav.indexOf("chrome") || o.$body.addClass("-is-safari")), o.initContent(), setTimeout(function () {
            $(".preloader-line").css("width", $(".preloader-line").width()).removeClass("-loading").delay(0).queue(function (e) {
                $(this).css("width", "100vw"), e();
            }).delay(640).queue(function (e) {
                o.isMobile || !$(".page").length || o.isCurrentPage("home") ? o.contentHeight = o.$body.height() : o.contentHeight = o.scrollbar.size.content.height;
                //o.changeTitle(document.title),

                o.$logo.find(".animated-word").animateCss("fadeInUp");
                o.initScroller(), $(".preloader").hide(), o.$body.removeClass("-loading").addClass("-loaded"), setTimeout(function () {
                    o.$body.removeClass("-loaded");
                }, 640), e();
            });
        }, 0);
    }, e.prototype.isCurrentPage = function (page) {
        return o.$body.hasClass("-page-" + page);
    }, e.prototype.updateController = function () {

        if (o.controller && o.controller.destroy(), o.controller = new ScrollMagic.Controller({
            refreshInterval: 20
        }), !o.isMobile && $(".page").length) {

            var scr_bar = window.Scrollbar;
            o.scrollbar = scr_bar.init($(".page")[0]), o.scrollbar.addListener(function (e) {
                o.controller.update();
            }), o.contentHeight = o.scrollbar.size.content.height;
        } else o.contentHeight = o.$page.height();
    }, e.prototype.initContent = function () {

        o.updateController(), $("video").each(function () {
            enableInlineVideo(this, {
                everywhere: /iPhone|iPad|iPod/i.test(navigator.userAgent)
            });
        }), o.initHonors(), o.initCountimator(), o.initFixedTabs(), setTimeout(function () {
            o.$body.hasClass("-loading") || setTimeout(function () {
                !o.isMobile && $(".page").length ? o.contentHeight = o.scrollbar.size.content.height : o.contentHeight = o.$page.height(), console.log(o.contentHeight);
                o.initScroller();
            }, 1050);
        }, 0);
    }, e.prototype.initFixedTabs = function () {
        if ($('.fixed__menu').length > 0) {

            var $fixedMenu = $('.fixed__menu');
            var s = void 0;

            $(document).on('scroll', function () {
                if ($(window).width() > 767) {
                    s = $(window).scrollTop();
                    if (s >= 68) {
                        $fixedMenu.addClass('is-fixed');
                    } else {
                        $fixedMenu.removeClass('is-fixed');
                    }
                }
            });
        }
    }, e.prototype.initScroller = function () {

        new ScrollMagic.Scene({
            triggerElement: ".page",
            offset: 0,
            duration: o.isCurrentPage("home") ? $('.header').height() + $('.welcome').height() - 200 : o.contentHeight - 200,
            triggerHook: 1
        }).setClassToggle(o.$body, "-hide-footer").addTo(o.controller);

        var scr;

        o.isCurrentPage("home") && $(window).on('scroll', function () {
            scr = $(window).scrollTop();
            if (scr >= 40 && scr + window.innerHeight < o.contentHeight - 78) {
                if (!o.$body.hasClass('-hide-footer')) o.$body.addClass('-hide-footer');
                if (o.$footer.hasClass('footer--main-bottom')) setTimeout(function () {
                    o.$footer.removeClass('footer--main-bottom');
                }, 400);
            } else {
                if (o.$body.hasClass('-hide-footer')) o.$body.removeClass('-hide-footer');
                if (scr + window.innerHeight >= o.contentHeight - 78) {
                    if (!o.$footer.hasClass('footer--main-bottom')) o.$footer.addClass('footer--main-bottom');
                }
            }
        }),

        /* o.isCurrentPage("case") && (new ScrollMagic.Scene({
             triggerElement: ".page",
             offset: 0,
             duration: $(window).height() - 60,
             triggerHook: 0
         }).setClassToggle(o.$body, "-is-white").addTo(o.controller),*/

        setTimeout(function () {
            $(".animated-title, .welcome, .ww__item, .works__sections, .serv-list, .works__item, .c-slider__slide, .c-form__form, .contact-list," + " .s-content__text, .header").each(function (e) {
                if (o.isMobile) var t = 1;else var t = .8;
                new ScrollMagic.Scene({
                    triggerElement: $(this),
                    triggerHook: t,
                    offset: 0 //o.isCurrentPage("home") ? $(this).height() / 2 - 175 : 0,
                }).setClassToggle($(this), "visible").addTo(o.controller);
            }), $(".serv-item p").each(function (e) {
                $(this).splitLines({
                    tag: '<div class="line"><div class="line__content">',
                    width: $(window).width() < 1023 && $(window).width() >= 768 ? 650 : '100%'
                });
            }), $(".s-content__text p").each(function (e) {
                $(this).addClass("s-text").splitLines({
                    tag: '<div class="line"><div class="line__content">'
                });
            }), $(".s-content__text h2").each(function (e) {
                var t = $('<span class="animated-title"><h2>' + $(this).html() + "</h2></span>");
                $(this).replaceWith(t);
            }), $(".s-content__text h3").each(function (e) {
                var t = $('<span class="animated-title"><h3>' + $(this).html() + "</h3></span>");
                $(this).replaceWith(t);
            }), setTimeout(function () {
                $(".about__text").each(function (e) {
                    $(this).addClass("visible").splitLines({
                        tag: '<div class="line"><div class="line__content">'
                    });
                });
            }, 200), $(".content-video").each(function (e) {
                var t = $(this);
                new ScrollMagic.Scene({
                    triggerElement: $(this),
                    duration: $(this).height() + o.$win.height(),
                    triggerHook: .8
                }).on("start", function () {
                    t.addClass("visible");
                    var e = t.find("video").get(0);
                    0 == e.paused ? e.pause() : e.play();
                }).on("end", function () {
                    var e = t.find("video").get(0);
                    0 == e.paused ? e.pause() : e.play();
                }).addTo(o.controller);
            });
        }, 640);
    },
    /*e.prototype.loadPage = function (e) {
        !o.animating && o.$body.hasClass("-is-menu-open") && (o.animating = !0,
            o.changeCurrentPage(window.projectsData[e].page),
            o.$body.removeClass("-hide-footer").addClass("-hide-content"),
            o.$page.show(),
            o.$promo.hide(),
            o.$case.hide(),
            o.$title.text(""),
            o.$contentContainer.show().html(window.projectsData[e].data),
            o.scrollbar ? o.scrollbar.setPosition(0, 0) : window.scrollTo(0, 0),
            o.$body.addClass("-menu-restore"),
            setTimeout(function () {
                window.App.initContent(),
                    o.$navMenu.addClass("no-animation"),
                    o.$body.removeClass("-menu-restore").removeClass("-is-white"),
                    o.animating = !1,
                    o.changeTitle(window.projectsData[e].title/!*, window.projectsData[e].subtitle*!/)
            }, 1280),
            $(".menu-toggle").click(),
            document.title = window.projectsData[e].title,
            window.history.pushState({
                url: e,
                html: window.projectsData[e].data,
                page: window.projectsData[e].page
            }, window.projectsData[e].title, e))
    }
    ,*/
    e.prototype.changeTitle = function (e, t) {
        if (t && "" != t) {
            if (o.isMobile) {
                i = !1;
                o.$logo.find(".animated-word").animateCss("fadeOutUp", function () {
                    o.$logo.html('<span class="animated-word">' + t + "</span>").find(".animated-word").animateCss("fadeInUp", function () {
                        i || setTimeout(function () {
                            o.$logo.find(".animated-word").animateCss("fadeOutUp", function () {
                                o.$logo.html('<span class="animated-word">' + e + "</span>").find(".animated-word").animateCss("fadeInUp");
                            });
                        }, 640);
                    });
                }), o.$win.scroll(function () {
                    i || (i = !0, o.hideTitle());
                });
            } else {
                o.$logo.find(".animated-word").animateCss("fadeOutUp", function () {
                    var e = t.split(" ");
                    o.$logo.empty(), $.each(e, function (e, t) {
                        o.$logo.append($("<span class='animated-word'>").html(t + "&nbsp;"));
                    }), o.$logo.find(".animated-word").animateCss("fadeInUp");
                }), setTimeout(function () {
                    o.$title.animateCss("fadeOutUp", function () {
                        o.$title.text(e).animateCss("fadeInUp");
                    });
                }, 200);
                var i = !1;
                o.scrollbar.addListener(function (e) {
                    i || (i = !0, o.hideTitle());
                }), $(".menu-toggle").click(function () {
                    i || (i = !0, o.hideTitle());
                });
            }
        } else o.$title.animateCss("fadeOutUp", function () {
            o.$title.text(e).animateCss("fadeInUp");
        });
    }, e.prototype.hideTitle = function (e) {
        o.$logo.find(".animated-word").animateCss("fadeOutUp", function () {
            o.$logo.html('<span class="animated-word">H&nbsp;</span><span class="animated-word">A</span>').find(".animated-word").animateCss("fadeInUp");
        });
    }, e.prototype.initCountimator = function () {
        $(".counter").each(function (el) {
            $(this).countimator();
        });
    }, e.prototype.initHonors = function () {
        if ($('.h-slider').length > 0) {
            var slider = $('.h-slider__scene');
            slider.each(function () {
                var navigation = $(this).parents('.s-section').find('.s-section__nav');
                var owl = $(this);
                owl.owlCarousel({
                    items: 2.2,
                    loop: true,
                    animateIn: 'fadeIn',
                    animateOut: 'fadeOut',
                    nav: true,
                    dots: false,
                    margin: 40,
                    navContainer: navigation,
                    navText: '',
                    navElement: 'a',
                    navContainerClass: 's-arrows',
                    navClass: ['s-arrows__arrow s-arrows__arrow--prev', 's-arrows__arrow s-arrows__arrow--next'],
                    responsive: {
                        0: {
                            items: 1.6,
                            mouseDrag: true,
                            center: true
                        },
                        768: {
                            items: 2
                        },
                        1024: {

                            mouseDrag: false
                        },
                        1200: {
                            center: false
                        },
                        1920: {
                            items: 1.8
                        }
                    }
                });

                $('body').on('click', '.a-slider__bg', function () {
                    owl.trigger('next.owl.carousel');
                });
            });
        }
    }, e;
}();

function Utils() {}

$(document).ready(function () {
    window.App = new AppClass();
});

Utils.prototype = {
    constructor: Utils,
    isEventSupported: function isEventSupported(e) {
        var t = document.createElement("div"),
            i = (e = "on" + e) in t;
        return i || (t.setAttribute(e, "return;"), i = "function" == typeof t[e]), t = null, i;
    },
    isInViewport: function isInViewport(e) {
        var t = e.offset().top,
            i = t + e.outerHeight(),
            n = $(window).scrollTop(),
            r = n + $(window).height();
        return n < i && t < r;
    }
};
var Utils = new Utils();
$.fn.extend({
    animateCss: function animateCss(e, t) {
        var i = function (e) {
            var t = {
                animation: "animationend",
                OAnimation: "oAnimationEnd",
                MozAnimation: "mozAnimationEnd",
                WebkitAnimation: "webkitAnimationEnd"
            };
            for (var i in t) {
                if (void 0 !== e.style[i]) return t[i];
            }
        }(document.createElement("div"));
        return this.addClass("animated " + e).one(i, function () {
            $(this).removeClass("animated " + e), "function" == typeof t && t();
        }), this;
    }
});