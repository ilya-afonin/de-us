<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
  <nav class="works__sections fixed__menu">
    <?php foreach($arResult as $arItem){ ?>
      <a class="link works__section<?=($arItem['ACTIVE'])?' works__section--active':''?>" href="<?=$arItem["LINK"]?>" role="button"><?=$arItem["TEXT"]?></a>
    <?php } ?>
    <div class="works__links-bg"></div>
  </nav>
<?php } ?>