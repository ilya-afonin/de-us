<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
  <div class="header__socials">
    <?php foreach($arResult as $arItem){ ?>
      <a class="header__soc-link" target="_blank" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
    <?php } ?>
  </div>
<?php } ?>