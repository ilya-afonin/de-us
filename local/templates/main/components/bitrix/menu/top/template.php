<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>

  <nav class="header__nav">
    <?php foreach($arResult as $arItem){ ?>
      <a class="link header__nav-link ajax-load" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
    <?php } ?>
  </nav>
<?php } ?>