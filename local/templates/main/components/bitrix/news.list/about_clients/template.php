<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="clients-slider">
  <div class="c-slider__scene owl-carousel">
    <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
      <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>
      <?if($k % 12 == 0):?>
          <?if($k !== 0 ):?>
            </div>
          <?endif;?>
      <div class="c-slider__slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
      <?endif;?>
        <div class="clients-item">
          <a class="clients-item__link">
            <?if(!empty($arItem['PREVIEW_PICTURE']['SRC'])):?>
              <img class="clients-item__img" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
            <?endif;?>
          </a>
        </div>
    <? endforeach; ?>
  </div>
</div>