<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="h-slider">
  <div class="h-slider__scene owl-carousel">
    <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
      <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>
      <div class="h-slider__slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <div class="h-slider__card">
          <div class="h-card">
            <div class="h-card__image-wrapper">
              <div class="h-card__title"><a href="#" <?=($arItem['PROPERTIES']['COLOR_BG']['VALUE'])?' style="color:'.$arItem['PROPERTIES']['COLOR_BG']['VALUE'].'"':'';?>><?=$arItem['NAME']?></a></div>
                <?if(!empty($arItem['PREVIEW_PICTURE']['SRC'])):?>
                  <img class="h-card__image" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
                <?endif;?>
            </div>
            <div class="h-card__title"><a href="#" <?=($arItem['PROPERTIES']['COLOR']['VALUE'])?' style="color:'.$arItem['PROPERTIES']['COLOR']['VALUE'].'"':'';?>><?=$arItem['NAME']?></a></div>
          </div>
        </div>
      </div>
    <? endforeach; ?>
  </div>
</div>