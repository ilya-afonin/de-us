<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$categories = array();

//Выводим теги
$arFilter = Array('IBLOCK_ID'=> 7, 'GLOBAL_ACTIVE'=>'Y', 'ACTIVE'=>'Y');
$db_list = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'CODE'));
while($ar_result = $db_list->GetNext()){
  $cat = array();
  $cat['ID'] = $ar_result['ID'];
  $cat['NAME'] = $ar_result['NAME'];
  $cat['CODE'] = $ar_result['CODE'];
  $categories[] = $cat;
}
foreach ($arResult['ITEMS'] as $k => $arItem) {
  foreach ($arItem['PROPERTIES']['TAGS']['VALUE'] as $tag){
    foreach($categories as $cat){
      if($tag === $cat['ID']) {
        $arResult['ITEMS'][$k]['tags'][] = $cat;
      }
    }
  }
}