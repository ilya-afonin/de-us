<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?if(count($arResult["ITEMS"])>0){?>
  <div class="works__list content_load">
  <?php
  if (!empty($_REQUEST['ajax'])){

      $APPLICATION->RestartBuffer();
  }
  ?>
      <?foreach ($arResult["ITEMS"] as $arItem):?>
      <?
      $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
      ?>

      <?if($arItem['PROPERTIES']['IS_SHOW_ADMIN']['VALUE'] == 'Y' && !$USER->IsAdmin()) continue; ?>

      <div class="works__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <a  href="<?=$arItem['DETAIL_PAGE_URL']?>" class="works__image-link">
          <div class="works__image" style="background-image: url(<?=$arItem['RESIZED_PICTURE']['SRC']?>)"></div>
        </a>
        <?if(count($arItem['PROPERTIES']['TAGS'])):?>
          <div class="works__tags">
            <?foreach($arItem['tags'] as $tag):?>
              <a class="works__tag" href="<?='/works/?tag='.$tag['CODE']?>" role="button"><?=$tag['NAME'];?></a>
            <?endforeach;?>
          </div>
        <?endif;?>
        <a class="link link--medium works__name" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
      </div>


      <?endforeach; ?>

      <?=$arResult["NAV_STRING"]?>

      <?php
      if (!empty($_REQUEST['ajax'])){
          die();
      }
      ?>
  </div>
<?php } else { ?>
    <div class="works__list content_load">
    <?php
    if (!empty($_REQUEST['ajax'])){
        $APPLICATION->RestartBuffer();
    }
    ?>
    <div class="works__item works__item--empty">
        <p>По вашему запросу ничего не найдено</p>
    </div>
    <?php
    if (!empty($_REQUEST['ajax'])){
        die();
    }
    ?>
</div>
<?php } ?>


