<?
// Check ajax...
if(!$_SERVER['HTTP_X_REQUESTED_WITH']){
    $APPLICATION->RestartBuffer();
    echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
    die();
}
// Check user agent...
if (!$_SERVER['HTTP_USER_AGENT']) {
    $APPLICATION->RestartBuffer();
    echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
    die();
}

$case_param = htmlentities($_POST['case_param']);

define("NO_KEEP_STATISTIC", true);
// for authorize and registration show errors (!! это проверить при удобной возможности)
$non_check_permissions_req = array('auth','auth_popup','reg','forgotpasswd','changepasswd');
if(!in_array($case_param,$non_check_permissions_req)){
    define("NOT_CHECK_PERMISSIONS", true);
}

include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;

// get arParams
$arParams = $_POST['component_arParams'];

// указываем для кого обязательны параметры
if(empty($arParams)){
    $arNeedParams = array('catalog_section','get_popup_full','get_popup_choise');
    if(in_array($case_param,$arNeedParams)){
        $case_param = 'empty_params';
    }
}

global $APPLICATION;

switch ($case_param):
    case 'search_result':
        $APPLICATION->RestartBuffer();
        ob_start();

        $requestParams = htmlentities($_SERVER["REQUEST_URI"]);
        $context = Bitrix\Main\Context::getCurrent();
        $request = $context->getRequest();
        $q = $request->get("q");

        $_POST['q'] = $q;
        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/search_title.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'SEARCH_RESULT' => $content,
            )
        );

        die();
        break;
    case 'auth':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/auth.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'AUTH' => $content,
            )
        );

        die();
        break;
    case 'auth_popup':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/auth_popup.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'AUTH_POPUP' => $content,
            )
        );

        die();
        break;
    case 'reg':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/reg.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'REG' => $content,
            )
        );

        die();
        break;
    case 'forgotpasswd':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/forgotpasswd.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'FORGOT_PASSWD' => $content,
            )
        );

        die();
        break;
    case 'changepasswd':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/changepasswd.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'CHANGE_PASSWD' => $content,
            )
        );

        die();
        break;
    case 'subscribe':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/subscribe.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'SUBSCRIBE' => $content,
            )
        );

        die();
        break;
    case 'catalog_section':
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "ajax",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arCurSection['ID'],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "Y",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => $arParams["SEF_MODE"],
                "SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
                //            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );

        $APPLICATION->RestartBuffer();
        ob_start();

        $intSectionID = 0;
        // обязательно объявляем константу CATALOG_SECTION_ID и CATALOG_IBLOCK_ID потому что они  используется для кастомной пагинации с баннерами
        if($arParams['CATALOG_SECTION_ID'] && $arParams['TYPE_SECTION']){
            define('CATALOG_SECTION_ID',$arParams['CATALOG_SECTION_ID']);
            define('TYPE_SECTION',$arParams['TYPE_SECTION']);
        }
        // for current url
        $_SERVER["REQUEST_URI"] = $arParams["REQUEST_URI"];
        $_SERVER["SCRIPT_NAME"] = $arParams["SCRIPT_NAME"];

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/catalog_section.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'CATALOG_SECTION' => $content
            )
        );

        die();
        break;
    case 'update_cart':
        $APPLICATION->RestartBuffer();
        ob_start();


        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/cart.php");
        $content = ob_get_contents();

        ob_end_clean();
        echo json_encode(
            array(
                'CART' => $content,
            )
        );

        die();
        break;
    case 'add_item_cart':
        $APPLICATION->RestartBuffer();
        ob_start();

        $productId = intval($_POST['requestParams']['buy_id']);
        $count = intval($_POST['requestParams']['count'])>0?intval($_POST['requestParams']['count']):1;
        // получаем количество макарон в подарок для товара
        Loader::includeModule("iblock");
        $arSelect = Array("ID", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_ID"=>array(4,12),"ID"=>$productId);
        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, array('nTopCount'=>1), $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arProps = $ob->GetProperties(array(),array('CODE'=>'BONUS_COUNT'));
        }
        $bonus_count = intval($arProps['BONUS_COUNT']['VALUE']);
        if($productId>0){
            $PRODUCT_PROPERTIES[] = array("NAME" => "Количество макарон в подарок", "VALUE" => $bonus_count);
            Loader::includeModule("catalog");
            if(Add2BasketByProductID($productId,$count,$PRODUCT_PROPERTIES)){
                require_once($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/small_cart.php");
            }
        }

        $content = ob_get_contents();

        ob_end_clean();
        echo json_encode(
            array(
                'SMALL_CART' => $content,
            )
        );

        die();
        break;
    case 'remove_item_cart':
        $APPLICATION->RestartBuffer();
        ob_start();

        $cart_item_id = intval($_POST['requestParams']['remove_item_cart']);
        if($cart_item_id>0){
            Loader::includeModule("sale");
            if(CSaleBasket::Delete($cart_item_id)){
                require_once($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/small_cart.php");
            }
        }

        $content = ob_get_contents();

        ob_end_clean();
        echo json_encode(
            array(
                'SMALL_CART' => $content,
            )
        );

        die();
        break;
    case 'get_popup_full':
        $APPLICATION->RestartBuffer();
        ob_start();

        $ElementCode = htmlentities($_POST['requestParams']['item_code']);

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/element_full.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'ELEMENT_FULL' => $content,
            )
        );

        die();
        break;
    case 'get_popup_choise':
        $APPLICATION->RestartBuffer();
        ob_start();

        $ElementCode = htmlentities($_POST['requestParams']['item_code']);

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/element_choise.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'ELEMENT_CHOISE' => $content,
            )
        );

        die();
        break;
    case 'add_items_cart_nabor':
        foreach($_POST["requestParams"] as $key_name=>$arSizeof){
            // собираем id товаров
            $parse_item = explode('_',$key_name);
            if($parse_item[1]){
                $item_id = intval($parse_item[1]);
                $sizeof_item = intval($arSizeof);

                $arItemsId[] = $item_id;
                $arItemsId_sizeof[$item_id] = $sizeof_item;
            }

            if($key_name == 'box'){
                $box_id = intval($arSizeof); // id упаковки
            }
        }

        Loader::includeModule("iblock");

        // получаем название упаковки
        if($box_id>0){
            $arSelect = Array("ID", "NAME","IBLOCK_ID");
            $arFilter = Array("IBLOCK_ID"=>14,"ID"=>$box_id, "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $boxName = $arFields['NAME'];
            }
        }

        // формируем id набора
        $nabor_id = mktime()-500000;

        // получаем количество макарон в подарок для товара
        if(!empty($arItemsId)){
            $arSelect = Array("ID", "IBLOCK_ID");
            $arFilter = Array("IBLOCK_ID"=>array(4),"ID"=>$arItemsId);
            $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties(array(),array('CODE'=>'BONUS_COUNT'));
                $arItemData[] = array(
                    'ID'=> $arFields['ID'],
                    'BONUS_COUNT'=> $arProps['BONUS_COUNT']['VALUE'],
                    'BOX_NAME' => $boxName,
                    'NABOR_ID' => $nabor_id,
                    'COUNT_ITEM' => $arItemsId_sizeof[$arFields['ID']]
                );
            }
        }

        if(is_array($arItemData) && sizeof($arItemData)>0){
            Loader::includeModule("catalog");
            // проверяем удалось ли добавить в корзину весь набор
            $flag_success = false;
            foreach($arItemData as $key=>$arItem){
                $PRODUCT_PROPERTIES = array();
                $PRODUCT_PROPERTIES[] = array("NAME" => "Количество макарон в подарок", "VALUE" => $arItem['BONUS_COUNT']);
                $PRODUCT_PROPERTIES[] = array("NAME" => "Упаковка", "VALUE" => $arItem['BOX_NAME']);
                $PRODUCT_PROPERTIES[] = array("NAME" => "Id набора", "VALUE" => $arItem['NABOR_ID']);
                if(Add2BasketByProductID($arItem['ID'],$arItem['COUNT_ITEM'],$PRODUCT_PROPERTIES)){
                    $arAddItemsId[] = $arItem['ID'];
                    $flag_success = true;
                }else{
                    $flag_success = false;
                    break;
                }
            }

            // если что то не добавилось удаляем весь набор
            if($flag_success == false && !empty($arAddItemsId)){
                foreach($arAddItemsId as $key=>$del_id){
                    CSaleBasket::Delete($del_id);
                }
                echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
                die();
            }else{
                // если все хорошо
                $APPLICATION->RestartBuffer();
                ob_start();

                require_once($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/small_cart.php");

                $content = ob_get_contents();

                ob_end_clean();
                echo json_encode(
                    array(
                        'SMALL_CART' => $content,
                    )
                );
            }
        }
        die();
        break;
    case 'senatorsFilter_alfabet':
        global $arFilterAlphabet;
        $arFilterAlphabet = array(
            'NAME' => $_REQUEST['letter'].'%'
        );
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "senators_ajax",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => ".default",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0=>"",1=>"",),
                "FILTER_NAME" => "arFilterAlphabet",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "3",
                "IBLOCK_TYPE" => "Info",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "6",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0=>"",1=>"REGION",),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "NAME",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "ASC"
            )
        );
        die();
        break;
    case 'senatorsFilter_region':
        global $arFilterRegion;
        $arFilterRegion = array(
            'PROPERTY_REGION' => $_REQUEST['region']
        );
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "senators_ajax",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => ".default",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0=>"",1=>"",),
                "FILTER_NAME" => "arFilterRegion",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "3",
                "IBLOCK_TYPE" => "Info",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "6",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0=>"",1=>"REGION",),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "NAME",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "ASC"
            )
        );
        die();
        break;
    case 'translationFilter_list':
        global $arFilterTranslation;
        $arFilterTranslation = array(
            ">=DATE_ACTIVE_FROM" => $_REQUEST['date']. '00:00',
            "<=DATE_ACTIVE_FROM" => $_REQUEST['date']. '23:59'
        );
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "translations_ajax",
            Array(
                "ADD_ELEMENT_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "FILTER_NAME" => "arFilterTranslation",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "N",
                "DETAIL_ACTIVE_DATE_FORMAT" => "Ymd",
                "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                "DETAIL_DISPLAY_TOP_PAGER" => "N",
                "DETAIL_FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "DETAIL_PAGER_SHOW_ALL" => "Y",
                "DETAIL_PAGER_TEMPLATE" => "",
                "DETAIL_PAGER_TITLE" => "Страница",
                "DETAIL_PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "DETAIL_SET_CANONICAL_URL" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "9",
                "IBLOCK_TYPE" => "live",
                "ACTIVE_DATE_FORMAT" => "Ymd|j F|D|H:i",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "LIST_FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "LIST_PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "MESSAGE_404" => "",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "NEWS_COUNT" => "100",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PREVIEW_TRUNCATE_LEN" => "",
                "SEF_FOLDER" => "/tv-guide/",
                "SEF_MODE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "USE_CATEGORIES" => "N",
                "USE_FILTER" => "N",
                "USE_PERMISSIONS" => "N",
                "USE_RATING" => "N",
                "USE_RSS" => "N",
                "USE_SEARCH" => "N",
                "USE_SHARE" => "N",
                "COMPONENT_TEMPLATE" => "live",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "SEF_URL_TEMPLATES" => array(
                    "news" => "",
                    "section" => "",
                    "detail" => "#ELEMENT_CODE#/",
                )
            )
        );
        die();
        break;
    case 'news_section':
        require($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/sort_flat.php");

        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "ajax",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => '',
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
                "INSTANT_RELOAD" => "N",
                "XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
                "SECTION_TITLE" => "NAME",	// Заголовок
                "SECTION_DESCRIPTION" => "DESCRIPTION",	// Описание
                "COMPONENT_TEMPLATE" => "visual_horizontal",
                "SECTION_CODE" => "",	// Код раздела
                "TEMPLATE_THEME" => "blue",	// Цветовая тема
                "DISPLAY_ELEMENT_COUNT" => "Y",	// Показывать количество
                "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                "PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
                "SORT_BY1" => $arParams['SORT_BY1'],
                "SORT_ORDER1" => $arParams['SORT_ORDER1'],
                "SORT_BY2" => $arParams['SORT_BY2'],
                "SORT_ORDER2" => $arParams['SORT_ORDER2'],
            ),
            false,
            array('HIDE_ICONS' => 'Y')
        );

        $APPLICATION->RestartBuffer();
        ob_start();

        $intSectionID = 0;

        // for current url
        $_SERVER["REQUEST_URI"] = $arParams["REQUEST_URI"];
        $_SERVER["SCRIPT_NAME"] = $arParams["SCRIPT_NAME"];

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/news_section.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'NEWS_SECTION' => $content
            )
        );

        die();
        break;
    case 'news_list':
        $APPLICATION->RestartBuffer();
        ob_start();

        // симв код раздела новостей
        $sectionCode = htmlentities($_POST['section_code']);

        // for current url
        $_SERVER["REQUEST_URI"] = $sectionCode>0?'/about/news/'.$sectionCode.'/':$arParams["REQUEST_URI"];
        // for current url - пример для англ версии на подразделе
//        if($lang == 'en'){
//            $_SERVER["REQUEST_URI"] = $sectionCode>0?'/en/news/'.$sectionCode.'/':$arParams["REQUEST_URI"];
//        }else{
//            $_SERVER["REQUEST_URI"] = $sectionCode>0?'/news/'.$sectionCode.'/':$arParams["REQUEST_URI"];
//        }

        $_SERVER["SCRIPT_NAME"] = $arParams["SCRIPT_NAME"];

        $requestParams = htmlentities($_SERVER["REQUEST_URI"]);
        $context = Bitrix\Main\Context::getCurrent();
        $request = $context->getRequest();
        $tag = $request->get("tag");
        if(!empty($tag) && $tag !='') {
            $search_tag = strip_tags($tag);
            $search_tag = htmlentities($search_tag, ENT_QUOTES, "UTF-8");
            $search_tag = htmlspecialchars($search_tag, ENT_QUOTES);
            global $arrFilterNewsList;
            $arrFilterNewsList['?TAGS'] = $search_tag;
        }

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/news_list.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'NEWS_LIST' => $content
            )
        );

        die();
        break;
    case 'news_list_favorites':
        $APPLICATION->RestartBuffer();
        ob_start();

        $_SERVER["SCRIPT_NAME"] = $arParams["SCRIPT_NAME"];

        $requestParams = htmlentities($_SERVER["REQUEST_URI"]);

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/news_list_favorites.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'NEWS_LIST_FAVORITES' => $content
            )
        );

        die();
        break;
    case 'feedback':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/feedback.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'FEEDBACK' => $content,
            )
        );

        die();
        break;
    case 'booking':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/booking.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'BOOKING' => $content,
            )
        );

        die();
        break;
    case 'feedback_call':
        $APPLICATION->RestartBuffer();
        ob_start();

        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/include_areas/popups/call.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'FEEDBACK_CALL' => $content,
            )
        );

        die();
        break;
    case 'generate_hash':
        $sessid = htmlentities($_POST['sessid']);
        $content = generateHash($sessid);

        echo json_encode(
            array(
                'HASH' => $content
            )
        );

        die();
        break;
    default:
        echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
        die();
        break;
endswitch;