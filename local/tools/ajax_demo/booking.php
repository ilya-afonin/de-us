<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "cakelabs:multiform",
    "booking",
    array(
        "COMPONENT_TEMPLATE" => "booking",
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "18",
        "SECTION_ID" => "0",
        "NOT_WRITE" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "EVENT_TYPE" => "CAKELABS_FEEDBACK",
        "EVENT_ID" => "10",
        "PROPERTY_FIELDS" => array(
            0 => "EMAIL",
            1 => "LINKED",
            2 => "NAME",
            3 => "PHONE",
            4 => "URL",
        ),
        "PROPERTY_FIELDS_REQUIRED" => array(
            0 => "EMAIL",
            1 => "LINKED",
            2 => "NAME",
            3 => "PHONE",
        ),
        "PROPERTY_FIELDS_HIDDEN" => array(
            0 => "LINKED",
            1 => "URL",
        ),
        "PROPERTY_FIELD_EMAIL" => array(
            0 => "EMAIL",
        ),
        "PROPERTY_FIELD_PHONE" => array(
            0 => "PHONE",
        ),
        "USE_MESSAGE_FIELD" => "N",
        "NAME_ELEMENT" => "NAME",
        "MESS_OK" => "Спасибо, Ваше сообщение успешно отправлено",
        "MAIL_TO" => Bitrix\Main\Config\Option::get("main", 'email_from'),
        "USE_PREMODERATION" => "Y",
        "USE_CAPTCHA" => "N"
    ),
    false
);