<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "small_cart", Array(
    "COMPONENT_TEMPLATE" => ".default",
    "PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
    "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
    "SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
    "SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
    "SHOW_EMPTY_VALUES" => "N",	// Выводить нулевые значения в пустой корзине
    "SHOW_PERSONAL_LINK" => "Y",	// Отображать персональный раздел
    "PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
    "SHOW_AUTHOR" => "Y",	// Добавить возможность авторизации
    "PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
    "PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
    "SHOW_PRODUCTS" => "Y",	// Показывать список товаров
    "POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
    "HIDE_ON_BASKET_PAGES" => "Y",	// Не показывать на страницах корзины и оформления заказа
),
    false
);
?>