<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent("cakelabs:multiform", "feedback", Array(
    "COMPONENT_TEMPLATE" => ".default",
    "IBLOCK_TYPE" => "content",	// Тип информационного блока
    "IBLOCK_ID" => "6",	// ID информационного блока
    "SECTION_ID" => "0",	// Раздел информационного блока
    "NOT_WRITE" => "N",	// Не записывать в инфоблок
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "EVENT_TYPE" => "CAKELABS_FEEDBACK",	// Тип почтового события(id типа)
    "EVENT_ID" => "8",	// ID почтового шаблона
    "PROPERTY_FIELDS" => array(	// Поля формы
        0 => "EMAIL",
        1 => "NAME",
        2 => "PHONE",
        3 => "URL",
    ),
    "PROPERTY_FIELDS_REQUIRED" => array(	// Обязательные поля формы
        0 => "MESSAGE_FIELD",
        1 => "EMAIL",
        2 => "NAME",
        3 => "PHONE",
    ),
    "PROPERTY_FIELDS_HIDDEN" => array(      // Скрытые поля формы
        0 => "URL",
    ),
    "PROPERTY_FIELD_EMAIL" => array(	// Использовать как email
        0 => "EMAIL",
    ),
    "PROPERTY_FIELD_PHONE" => array(	// Использовать как телефон
        0 => "PHONE",
    ),
    "USE_MESSAGE_FIELD" => "Y",	// Выводить поле для сообщения
    "MESSAGE_FIELD_NAME" => "Сообщение",	// Название поля для сообщения
    "NAME_ELEMENT" => "NAME",	// Устанавливать название элемента из
    "MESS_OK" => "Спасибо, Ваше сообщение успешно отправлено",	// Сообщение об успешной отправке
    "MAIL_TO" => Bitrix\Main\Config\Option::get("main", 'email_from'),	// Кому отправлять
    "USE_PREMODERATION" => "Y",	// Включить премодерацию сообщений
    "USE_CAPTCHA" => "N",	// Включить защиту от спама (CAPTCHA)
),
    false
);