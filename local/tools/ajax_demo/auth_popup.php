<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "bitrix:system.auth.form",
    "",
    Array(
        'AUTH_RESULT' => $APPLICATION->arAuthResult,
        'SHOW_ERRORS' => 'Y'
    ),
    false
);
?>