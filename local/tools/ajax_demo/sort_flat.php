<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// sort
$arSortBy = array('PROPERTY_SQUARE','PROPERTY_COST');
$arSortOrder = array('asc','desc');
$context = Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
$sortBy = $request->get("sort");
$sortOrder = $request->get("order");

// сортируем по площади
if($sortBy && in_array($sortBy,$arSortBy) && $sortOrder && in_array($sortOrder,$arSortOrder)){
    if($sortBy == 'PROPERTY_SQUARE'){
        $arParams['SORT_BY1'] = $sortBy;
        $arParams['SORT_ORDER1'] = $sortOrder;
        $arParams['SORT_BY2'] = 'PROPERTY_COST';
        $arParams['SORT_ORDER2'] = $sortOrder;
    }elseif($sortBy == 'PROPERTY_COST'){
        $arParams['SORT_BY1'] = $sortBy;
        $arParams['SORT_ORDER1'] = $sortOrder;
        $arParams['SORT_BY2'] = 'PROPERTY_SQUARE';
        $arParams['SORT_ORDER2'] = $sortOrder;
    }
}else{
    $arParams['SORT_BY1'] = 'PROPERTY_COST';
    $arParams['SORT_ORDER1'] = 'asc';
    $arParams['SORT_BY2'] = 'PROPERTY_SQUARE';
    $arParams['SORT_ORDER2'] = 'asc';
}