<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($component)){
    $arParams["COMPONENT"] = $component;
    // передаем симв код элемента (ЧПУ)
    if(!$ElementCode){
        $ElementCode = $arResult["VARIABLES"]["ELEMENT_CODE"];
    }
}else{
    $arParams["COMPONENT"] = false;
}

$APPLICATION->IncludeComponent(
    "bitrix:catalog.element",
    "full",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"]?$arParams["IBLOCK_TYPE"]:"catalog",
        "IBLOCK_ID" => $arParams["IBLOCK_ID"]?$arParams["IBLOCK_ID"]:"4",
        "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"]?$arParams["DETAIL_PROPERTY_CODE"]:array(
            0 => "VKUS",
            1 => "POVOD",
            2 => "NO_NACHINKA",
            3 => "KIDS",
            4 => "STAMP",
            5 => "",
        ),
        "META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"]?$arParams["DETAIL_META_KEYWORDS"]:"-",
        "META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"]?$arParams["DETAIL_META_DESCRIPTION"]:"-",
        "BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"]?$arParams["DETAIL_BROWSER_TITLE"]:"-",
        "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"]?$arParams["DETAIL_SET_CANONICAL_URL"]:"N",
        "BASKET_URL" => $arParams["BASKET_URL"]?$arParams["BASKET_URL"]:"/personal/cart/",
        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"]?$arParams["ACTION_VARIABLE"]:"action",
        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"]?$arParams["PRODUCT_ID_VARIABLE"]:"id",
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"]?$arParams["SECTION_ID_VARIABLE"]:"SECTION_ID",
        "CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"]?$arParams["PRODUCT_QUANTITY_VARIABLE"]:"",
        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"]?$arParams["PRODUCT_PROPS_VARIABLE"]:"prop",
        "CACHE_TYPE" => $arParams["CACHE_TYPE"]?$arParams["CACHE_TYPE"]:"A",
        "CACHE_TIME" => $arParams["CACHE_TIME"]?$arParams["CACHE_TIME"]:"36000000",
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"]?$arParams["CACHE_GROUPS"]:"Y",
        "SET_TITLE" => $arParams["SET_TITLE"]?$arParams["SET_TITLE"]:"Y",
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"]?$arParams["SET_LAST_MODIFIED"]:"N",
        "MESSAGE_404" => $arParams["MESSAGE_404"]?$arParams["MESSAGE_404"]:"",
        "SET_STATUS_404" => $arParams["SET_STATUS_404"]?$arParams["SET_STATUS_404"]:"Y",
        "SHOW_404" => $arParams["SHOW_404"]?$arParams["SHOW_404"]:"N",
        "FILE_404" => $arParams["FILE_404"],
        "PRICE_CODE" => $arParams["PRICE_CODE"]?$arParams["PRICE_CODE"]:array(
            0 => "BASE",
        ),
        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"]?$arParams["USE_PRICE_COUNT"]:"N",
        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"]?$arParams["SHOW_PRICE_COUNT"]:"1",
        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"]?$arParams["PRICE_VAT_INCLUDE"]:"Y",
        "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"]?$arParams["PRICE_VAT_SHOW_VALUE"]:"N",
        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY']?$arParams['USE_PRODUCT_QUANTITY']:"N",
        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"]?$arParams["PRODUCT_PROPERTIES"]:array(),
        "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : "Y"),
        "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : "N"),
        "LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"]?$arParams["LINK_IBLOCK_TYPE"]:"",
        "LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"]?$arParams["LINK_IBLOCK_ID"]:"",
        "LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"]?$arParams["LINK_PROPERTY_SID"]:"",
        "LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"]?$arParams["LINK_ELEMENTS_URL"]:"link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",

        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"]?$arParams["OFFERS_CART_PROPERTIES"]:array(
            0 => "SIZEOF",
        ),
        "OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"]?$arParams["DETAIL_OFFERS_FIELD_CODE"]:array(
            0 => ""
        ),
        "OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"]?$arParams["DETAIL_OFFERS_PROPERTY_CODE"]:array(
            0 => "SIZEOF"
        ),
        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"]?$arParams["OFFERS_SORT_FIELD"]:"sort",
        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"]?$arParams["OFFERS_SORT_ORDER"]:"asc",
        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"]?$arParams["OFFERS_SORT_FIELD2"]:"id",
        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"]?$arParams["OFFERS_SORT_ORDER2"]:"desc",

        "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"]?$arResult["VARIABLES"]["ELEMENT_ID"]:$arParams["VARIABLES_ELEMENT_ID"],
        "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]?$arResult["VARIABLES"]["ELEMENT_CODE"]:$ElementCode,
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]?$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]:"/catalog/#SECTION_CODE#/",
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"]?$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"]:"/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY']?$arParams['CONVERT_CURRENCY']:"N",
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"]?$arParams["HIDE_NOT_AVAILABLE"]:"N",
        'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER']?$arParams['USE_ELEMENT_COUNTER']:"N",
        'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED']?$arParams['SHOW_DEACTIVATED']:"N",
        "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"]?$arParams["USE_MAIN_ELEMENT_SECTION"]:"N",

        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP']?$arParams['ADD_PICT_PROP']:"-",
        'LABEL_PROP' => $arParams['LABEL_PROP']?$arParams['LABEL_PROP']:"-",
        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP']?$arParams['OFFER_ADD_PICT_PROP']:"-",
        'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS']?$arParams['OFFER_TREE_PROPS']:array(
            0 => "SIZEOF",
        ),
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT']?$arParams['SHOW_DISCOUNT_PERCENT']:"N",
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE']?$arParams['SHOW_OLD_PRICE']:"N",
        'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY']?$arParams['DETAIL_SHOW_MAX_QUANTITY']:"N",
        'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY']?$arParams['MESS_BTN_BUY']:"Купить",
        'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET']?$arParams['MESS_BTN_ADD_TO_BASKET']:"В корзину",
        'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
        'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE']?$arParams['MESS_BTN_COMPARE']:"Сравнение",
        'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE']?$arParams['MESS_NOT_AVAILABLE']:"Нет в наличии",
        'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING']?$arParams['DETAIL_USE_VOTE_RATING']:"N",
        'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
        'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS']?$arParams['DETAIL_USE_COMMENTS']:"N",
        'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
        'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
        'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
        'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
        'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
        'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
        'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
        'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
        'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
        'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : "Y"),
        'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : "N"),
        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
        "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : "Y"),
        "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : "N"),
        "DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : "E"),
        "DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : "IMG"),
        'ADD_TO_BASKET_ACTION' => $basketAction,
        'SHOW_CLOSE_POPUP' => (isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : "N"),
        'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : "N"),
        'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
        'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y'),
        'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : "-"),
        'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : "N"),
        'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : "N"),

        "USE_GIFTS_DETAIL" => $arParams['USE_GIFTS_DETAIL']?: 'N',
        "USE_GIFTS_MAIN_PR_SECTION_LIST" => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST']?: 'Y',
        "GIFTS_SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT']?:'Y',
        "GIFTS_SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE']?:"Y",
        "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT']?:"3",
        "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE']?:"N",
        "GIFTS_DETAIL_TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT']?:"Подарок",
        "GIFTS_DETAIL_BLOCK_TITLE" => $arParams["GIFTS_DETAIL_BLOCK_TITLE"]?:"Выберите один из подарков",
        "GIFTS_SHOW_NAME" => $arParams['GIFTS_SHOW_NAME']?:"Y",
        "GIFTS_SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE']?:"Y",
        "GIFTS_MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY']?:"Выбрать",

        "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT']?:"3",
        "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE']?:"Выберите один из товаров, чтобы получить подарок",
    ),
    $component
);?>