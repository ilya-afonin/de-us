<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent("cakelabs:search.title", "mera", Array(
    "COMPONENT_TEMPLATE" => "mera",
    "NUM_CATEGORIES" => "1",	// Количество категорий поиска
    "TOP_COUNT" => "5",	// Количество результатов в каждой категории
    "ORDER" => "date",	// Сортировка результатов
    "USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
    "CHECK_DATES" => "N",	// Искать только в активных по дате документах
    "SHOW_OTHERS" => "N",	// Показывать категорию "прочее"
    "PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
    "SHOW_INPUT" => "Y",	// Показывать форму ввода поискового запроса
    "INPUT_ID" => "title-search-input",	// ID строки ввода поискового запроса
    "CONTAINER_ID" => "title-search",	// ID контейнера, по ширине которого будут выводиться результаты
    "CATEGORY_0_TITLE" => "",	// Название категории
    "CATEGORY_0" => array(
        0 => "iblock_catalog",
        1 => "iblock_content",
    )
),
    false
);