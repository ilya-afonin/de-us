<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "bitrix:system.auth.registration",
    "flat",
    Array('AUTH_RESULT' => $APPLICATION->arAuthResult),
    false
);
?>