<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent(
    "cakelabs:sale.fastorder",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "USE_CAPTCHA" => "N",
        "OK_TEXT" => "Спасибо! Мы свяжемся с вами в ближайшее время!",
        "EMAIL_TO" => COption::GetOptionString("main", "email_from"),
        "REQUIRED_FIELDS" => array(
            0 => "NAME",
            1 => "EMAIL",
        ),
        "EVENT_MESSAGE_ID" => array(
        )
    ),
    false
);
?>
