<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->IncludeComponent("bitrix:subscribe.edit", "subscribe", Array(
    "SHOW_HIDDEN" => "N",	// Показать скрытые рубрики подписки
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "ALLOW_ANONYMOUS" => "Y",	// Разрешить анонимную подписку
    "SHOW_AUTH_LINKS" => "N",	// Показывать ссылки на авторизацию при анонимной подписке
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "COMPONENT_TEMPLATE" => ".default"
),
    false
);?>