<?
// Check ajax...
if(!$_SERVER['HTTP_X_REQUESTED_WITH']){
    $APPLICATION->RestartBuffer();
    echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
    die();
}
// Check user agent...
if (!$_SERVER['HTTP_USER_AGENT']) {
    $APPLICATION->RestartBuffer();
    echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
    die();
}

$case_param = htmlentities($_POST['case_param']);

define("NO_KEEP_STATISTIC", true);
// for authorize and registration show errors
$non_check_permissions_req = array('auth','auth_popup','reg','forgotpasswd','changepasswd');
if(!in_array($case_param,$non_check_permissions_req)){
    define("NOT_CHECK_PERMISSIONS", true);
}

include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;

// get arParams
$arParams = $_POST['component_arParams'];

// указываем для кого обязательны параметры
if(empty($arParams)){
    $arNeedParams = array('catalog_section','get_popup_full','get_popup_choise');
    if(in_array($case_param,$arNeedParams)){
        $case_param = 'empty_params';
    }
}

global $APPLICATION;

switch ($case_param):
    case 'search_result':
        $APPLICATION->RestartBuffer();
        ob_start();

        $requestParams = htmlentities($_SERVER["REQUEST_URI"]);
        $context = Bitrix\Main\Context::getCurrent();
        $request = $context->getRequest();
        $q = $request->get("q");

        $_POST['q'] = $q;
        include($_SERVER["DOCUMENT_ROOT"]."/local/tools/ajax/search_title.php");

        $content = ob_get_contents();
        ob_end_clean();
        echo json_encode(
            array(
                'SEARCH_RESULT' => $content,
            )
        );

        die();
        break;
    default:
        echo(json_encode(array("status" => false,'answer' => "Неверный запрос")));
        die();
        break;
endswitch;