<?

class Debug {
    /**
     * Log message with label
     *  для вывода в лог файл по дням года с иерархией папок
     * @param mixed $message
     * @param string $label
     * @param string $path
     */
    public static function lm($message, $label = '', $path = '') {
        $backTrace = debug_backtrace();
        $logPathPart = array(
            'logs',
            date('Y'),
            date('m'),
            date('d')
        );

        $logPath = $_SERVER['DOCUMENT_ROOT'].'/';

        foreach ($logPathPart as $part) {
            $logPath .= $part.'/';

            if (!file_exists($logPath)) {
                mkdir($logPath, 0700);
            }

            if ($part == 'logs') {
                $htAccess = fopen($logPath.'.htaccess', 'w');
                fwrite($htAccess, 'Deny from all');
                fclose($htAccess);
            }
        }

        $path = $logPath.(mb_strlen($path) == 0 ? 'log.txt' : $path);
        $file = fopen($path, 'a+');
        fwrite($file, date('r', time())."\t".$label.': '.print_r($message, true)." (line: ".$backTrace[0]['line'].", file: ".$backTrace[0]['file'].")\n");
        fclose($file);
    }

    /**
     * Object to array
     *
     * @param object $object
     *
     * @return array
     */
    public static function ota($object) {
        if (!is_object($object) && !is_array($object)) {
            return $object;
        }

        if (is_object($object)) {
            $object = get_object_vars($object);
        }

        return array_map('objectToArray', $object);
    }

    /**
     * Вывод переменной в консоль
     *
     * @param mixed $var
     * @param string $label
     * @param bool $useBackTrace
     * @param bool $toString
     *
     * @return string
     */
    public static function dtc ($var, $label = '', $useBackTrace = true, $toString = false) {
        if($var == ''){
            return $var.' empty';
        }
        $backTrace = array ();
        if ($useBackTrace) {
            $backTrace = debug_backtrace ();
        }

        $json = json_encode (unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($var))));
        if ($json)
            $js = '<script type="text/javascript"> 
console.log('.(empty($label) ? '' : '\''.$label.'\',').$json.');//
'.($useBackTrace ? 'console.log("line: '.$backTrace[0]['line'].', file: '.$backTrace[0]['file'].'")' : '').'; 
</script>';
        else {
            $js = '<script type="text/javascript"> 
console.log('.(empty($label) ? '' : '\''.$label.'\',').json_last_error_msg().');//
'.($useBackTrace ? 'console.log("line: '.$backTrace[0]['line'].', file: '.$backTrace[0]['file'].'")' : '').'; 
</script>';
        }

        if (!$toString) {
            echo $js;

            return '';
        } else {
            return $js;
        }
    }

    /**
     * @param mixed $value
     * @param string $label
     */
    public static function d($value, $label) {
        echo '<pre>'.$label.'='.print_r($value, true).'</pre>';
    }

    //region Time debug
    static $time = array();

    /**
     * Return response time array
     *
     * @return array
     */
    private static function getResponseArray() {
        return array(
            'start' => 0,
            'end' => 0,
            'time' => 0
        );
    }

    /**
     * Return time
     *
     * @return mixed
     */
    private static function getTime() {
        $time = explode(' ', microtime());
        return $time[0] + $time[1];
    }

    /**
     * Start timer
     *
     * @param string|int $id
     */
    static function start($id = 0) {
        self::$time[$id] = self::getResponseArray();
        self::$time[$id]['start'] = self::getTime();
    }

    /**
     * Stop timer and return time array
     *
     * @param string|int $id
     * @return mixed
     */
    static function stop($id = 0) {
        self::$time[$id]['end'] = self::getTime();
        self::$time[$id]['time'] = self::$time[$id]['end'] - self::$time[$id]['start'];
        return self::$time[$id];
    }
    //endregion
}