<?
define('PAGE','works');
if (!empty($_REQUEST['ajax']))
  require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
else
  require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Кейсы");
?>
    <div class="container">
        <div class="works-page">
            <div class="works__sect-wrapper">
              <? $APPLICATION->IncludeComponent(
                  "bitrix:menu",
                  "work_menu",
                  Array(
                      "ALLOW_MULTI_SELECT" => "N",
                      "CHILD_MENU_TYPE" => "",
                      "DELAY" => "N",
                      "MAX_LEVEL" => "1",
                      "MENU_CACHE_GET_VARS" => array(0 => "", 1 => "", 2 => "",),
                      "MENU_CACHE_TIME" => "3600000",
                      "MENU_CACHE_TYPE" => "A",
                      "MENU_CACHE_USE_GROUPS" => "Y",
                      "ROOT_MENU_TYPE" => "razdel",
                      "USE_EXT" => "Y"
                  )
              ); ?>
            </div>

            <?
            CModule::IncludeModule('iblock');

            $GLOBALS['arFilterPortfolio'] = array();

            if (!empty($_REQUEST["tag"])) {
                $arFilter = Array('IBLOCK_ID'=> 7, 'GLOBAL_ACTIVE'=>'Y', 'ACTIVE'=>'Y', 'CODE' => $_REQUEST["tag"]);
                $db_list = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('IBLOCK_ID','ID','CODE'));
                if($ar_result = $db_list->GetNext()) {
                    $GLOBALS['arFilterPortfolio'] = array(
                        "PROPERTY_TAGS" => $ar_result["ID"]
                    );
                }
            }

            $APPLICATION->IncludeComponent("deus:elements.list", "portfolio_list", Array(
                "IBLOCK_TYPE" => "content",  // Тип инфоблока
                "IBLOCK_ID" => "1",  // ID инфоблока
                "ITEMS_LIMIT" => "100",  // Количество элементов
                "CACHE_TYPE" => ($USER->IsAdmin())?"A":'N',  // Тип кеширования
                "CACHE_TIME" => "3600002",  // Время кеширования (сек.)
                "FILTER_NAME" => "arFilterPortfolio",
                'SORT_FIELD' => 'SORT',
                'SORT_ORDER' => 'ASC',
                'SECTION_CODE' => '',
                "FIELD_CODE" => array(  // Поля инфоблока
                    0 => "ID",
                    1 => "CODE",
                    2 => "NAME",
                    3 => "SORT",
                    4 => "PREVIEW_TEXT",
                    5 => "PREVIEW_PICTURE",
                    6 => "DETAIL_TEXT",
                    7 => "IBLOCK_ID",
                    8 => "IBLOCK_SECTION_ID",
                ),
                "PROPERTY_CODE" => array(  // Свойства инфоблока
                    0 => "PARALAX_MAIN_PHOTO",
                    1 => "PARALAX_MAIN_VIDEO",
                    2 => "TAGS",
                    3 => "",
                ),
                "COMPONENT_TEMPLATE" => ".default",
                "COLOR" => "FFFF00",  // Выбор цвета
                "IMG_WIDTH" => "600",  // Ширина миниатюры
                "IMG_HEIGHT" => "480",  // Высота миниатюры
                "ACTIVE_DATE_FORMAT" => "j f",  // Формат даты
                "SET_META_KEYWORDS" => "Y",
                "META_KEYWORDS" => "-",
                "SET_META_DESCRIPTION" => "Y",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "SET_TITLE" => "Y",
            ),
                false
            ); ?>

        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>