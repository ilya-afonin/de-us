<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="case__index-wrapper">
    <div class="container">
        <div class="case__index">
            <div class="case__index-arrow"></div>
            <div class="case__index-title wow flipInX">Главный экран</div>
            <div class="case__index__img wow fadeIn" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/index_img.png" alt="Главная">
            </div>
        </div>
    </div>
    <div class="case__index-bg"></div>
</div>
<div class="container">
    <div class="case__pages">
        <div class="case__page">
            <div class="case__page-img wow fadeInLeft" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/product.png" alt="Продукт">
            </div>
            <div class="case__page-text wow fadeIn" data-wow-duration="2s" data-wow-offset="200">
                <div class="case__page-title">продукты</div>
                <div class="case__page-desc">Все продукты компании представлены на главной странице и доступны для пользователя в один клик.
                </div>
            </div>
        </div>
        <div class="case__page case__page-reverse">
            <div class="case__page-img wow fadeInRight" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/advantage.png" alt="Преимущества">
            </div>
            <div class="case__page-text wow fadeIn" data-wow-duration="2s" data-wow-offset="200">
                <div class="case__page-title">преимущества</div>
                <div class="case__page-desc">Для удобства пользователя мы вынесли несколько преимуществ, чтобы создать правильное впечатление о сервисах.
                </div>
            </div>
        </div>
        <div class="case__page">
            <div class="case__page-text wow fadeIn" data-wow-duration="2s" data-wow-offset="200">
                <div class="case__page-title">контакты</div>
                <div class="case__page-desc">Простое и понятное для пользователя решение без лишних отвлекающих элементов.
                </div>
            </div>
            <div class="case__page-img wow fadeInLeft" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/contacts.png" alt="Контакты">
            </div>
        </div>
        <div class="case__page case__page-reverse">
            <div class="case__page-text wow fadeIn" data-wow-duration="2s" data-wow-offset="200">
                <div class="case__page-title">ближайшие события</div>
                <div class="case__page-desc">Все главные события из жизни компании доступны на главной странице в информационном блоке в виде слайдера.
                </div>
            </div>
            <div class="case__page-img wow fadeInRight" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/events.png" alt="События">
            </div>
        </div>
        <div class="case__page">
            <div class="case__page-img case__page-img-news wow fadeInLeft" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/news1.png" alt="новость 1">
            </div>
            <div class="case__page-text case__page-text-news wow fadeIn" data-wow-duration="2s" data-wow-offset="200">
                <div class="case__page-title">Карточки новостей</div>
                <div class="case__page-desc">Новости компании реализованы в виде карточек на главной странице сайта.
                </div>
            </div>
            <div class="case__page-img case__page-img-news case__page-img--two wow fadeInRight" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/news2.png" alt="новость 2">
            </div>
        </div>
    </div>
</div>
<div class="case__team-wrapper">
    <div class="container">
        <div class="case__team">
            <div class="case__team-arrow"></div>
            <div class="case__team-title wow flipInX">Блок Команда</div>
            <div class="case__team__img wow fadeIn" data-wow-offset="200">
                <img src="/portfolio/sola/images/content/company.png" alt="Команда">
            </div>
        </div>
    </div>
    <div class="case__team-bg">
        <div class="container">
            <div class="case__team-bg-img">
                <img src="/portfolio/sola/images/content/team_bg.svg" alt="">
            </div>
        </div>
    </div>
</div>
<div class="case__adaptive-wrapper">
    <div class="container">
        <div class="case__adaptive">
            <div class="case__adaptive-arrow"></div>
            <div class="case__adaptive-title wow flipInX">Пример адаптивных страниц</div>
            <div class="case__adaptive__images">
                <div class="case__adaptive__img wow fadeInUp" data-wow-offset="200">
                    <img src="/portfolio/sola/images/content/adaptive1.png" alt="Адаптивная 1">
                </div>
                <div class="case__adaptive__img wow fadeInUp" data-wow-offset="200" data-wow-delay="100ms">
                    <img src="/portfolio/sola/images/content/adaptive2.png" alt="Адаптивная 2">
                </div>
                <div class="case__adaptive__img wow fadeInUp" data-wow-offset="200" data-wow-delay="200ms">
                    <img src="/portfolio/sola/images/content/adaptive3.png" alt="Адаптивная 3">
                </div>
            </div>
        </div>
    </div>
    <div class="case__adaptive-bg">
        <div class="container">
            <div class="case__adaptive-bg-img"></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="case__color-fonts">
        <div class="case__colors">
            <div class="case__colors-title wow fadeIn">Цветовая палитра</div>
            <div class="case__colors-wrapper">
                <div class="case__color case__color1 wow fadeIn" data-wow-delay="50ms">
                    <div class="case__color-img"></div>
                    <div class="case__color-title">#f7901a</div>
                </div>
                <div class="case__color case__color2 wow fadeIn" data-wow-delay="100ms">
                    <div class="case__color-img"></div>
                    <div class="case__color-title">#6a707f</div>
                </div>
                <div class="case__color case__color3 wow fadeIn" data-wow-delay="150ms">
                    <div class="case__color-img"></div>
                    <div class="case__color-title">#24292f</div>
                </div>
            </div>
        </div>
        <div class="case__fonts">
            <div class="case__fonts-title">Шрифты</div>
            <div class="case__fonts-wrapper">
                <div class="case-font case__font--ubuntu wow fadeIn" data-wow-delay="50ms">
                    <div class="case__font-name">Ubuntu</div>
                    <div class="case__font-types">
                        <div class="case__font-type case__font-type--m">Medium</div>
                        <div class="case__font-type case__font-type--b">Bold</div>
                    </div>
                </div>
                <div class="case-font case__font--gotham wow fadeIn" data-wow-delay="100ms">
                    <div class="case__font-name">Gotham Pro</div>
                    <div class="case__font-types">
                        <div class="case__font-type case__font-type--b">Bold</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="case__dimensions">
        <div class="case__dimensions-title">Сетка</div>
        <div class="case__dimensions-wrapper">
            <div class="row">
                <div class="case__dimension col-xs-12">
                    <div class="case__dimension-title-wrapper wow flipInX">
                        <div class="case__dimension-title">Ноутбук</div>
                        <div class="case__dimension-line wow slideInLeft"></div>
                        <div class="case__dimension-dpi">1366 px</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="case__dimension col-xs-8">
                    <div class="case__dimension-title-wrapper wow flipInX">
                        <div class="case__dimension-title">Планшет</div>
                        <div class="case__dimension-line wow slideInLeft"></div>
                        <div class="case__dimension-dpi">768 px</div>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="case__dimension col-xs-4">
                <div class="case__dimension-title-wrapper wow flipInX">
                    <div class="case__dimension-title">Мобильная</div>
                    <div class="case__dimension-line wow slideInLeft"></div>
                    <div class="case__dimension-dpi">375 px</div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="case__bg">
    <div class="container">
        <img src="/portfolio/sola/images/content/macbook.png" alt="Sola" class="wow fadeIn" data-wow-offset="200" data-wow-duration="1s">
    </div>
</div>

