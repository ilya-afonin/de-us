<div class="container">
  <div class="case__index">
    <div class="case__index-img wow fadeIn" data-wow-offset="200">
      <img src="/portfolio/pobeda/images/content/main.png" alt="Победа">
    </div>
    <div class="case__index-points">
      <div class="case__index-point case__index-point1 wow slideInLeft">
        <div class="case__index-point-title">Экран со слайдером</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point2 wow slideInLeft">
        <div class="case__index-point-title">Преимущества</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point3 wow slideInLeft">
        <div class="case__index-point-title">Каталог автомобилей</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point4 wow slideInLeft">
        <div class="case__index-point-title">Условия аренды</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point5 wow slideInLeft">
        <div class="case__index-point-title">Как все происходит</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point6 wow slideInLeft">
        <div class="case__index-point-title">О компании</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point7 wow slideInLeft">
        <div class="case__index-point-title">Метки на карте</div>
        <div class="case__index-point-line"></div>
      </div>
      <div class="case__index-point case__index-point8 wow slideInLeft">
        <div class="case__index-point-title">Контактная информация</div>
        <div class="case__index-point-line"></div>
      </div>
    </div>
  </div>
</div>
<div class="case__grid-ui">
  <div class="container">
    <div class="case__grid">
      <div class="case__header wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
        <div class="case__head-left">
          <div class="case__head-title-wrapper">
            <div class="case__head-title">Сетка & UI элементы</div>
          </div>
            <div class="case__head-desc">Мы использовали довольно популярную сетку.
                Благодаря этому мы смогли реализовать дизайн блоков в виде карточек.
                Это отличное решение для пользователя, так как дизайн удобен и логичен.
            </div>
        </div>
      </div>
      <div class="case__dimensions">
        <div class="row">
          <div class="case__dimension case__dimension1 col-xs-12">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-dpi">
                <div class="case__dimension-digit">1170</div>
                <div class="case__dimension-px">px</div>
              </div>
              <div class="case__dimension-title">Компьютер</div>
            </div>
            <div class="case__dimension-line wow slideInLeft"></div>
          </div>
        </div>
        <div class="row">
          <div class="case__dimension case__dimension2 col-xs-9">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-dpi">
                <div class="case__dimension-digit">870</div>
                <div class="case__dimension-px">px</div>
              </div>
              <div class="case__dimension-title">Планшет</div>
            </div>
            <div class="case__dimension-line wow slideInLeft"></div>
          </div>
        </div>
        <div class="row">
          <div class="case__dimension case__dimension3 col-xs-6">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-dpi">
                <div class="case__dimension-digit">560</div>
                <div class="case__dimension-px">px</div>
              </div>
              <div class="case__dimension-title">Мини планшет</div>
            </div>
            <div class="case__dimension-line wow slideInLeft"></div>
          </div>
        </div>
        <div class="row">
          <div class="case__dimension case__dimension4 col-xs-3">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-dpi">
                <div class="case__dimension-digit">270</div>
                <div class="case__dimension-px">px</div>
              </div>
              <div class="case__dimension-title">Телефон</div>
            </div>
            <div class="case__dimension-line wow slideInLeft"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="case__ui">
  <div class="container">
    <div class="case__ui_bg">
      <div class="case__ui-items">
        <div class="case__ui-item wow fadeInLeft">
          <div class="case__ui-item-img">
            <img src="/portfolio/pobeda/images/content/item-1.png" alt="">
          </div>
          <div class="case__ui-item-title-wrapper">
            <div class="case__ui-item-title">Карточка преимущества</div>
            <div class="case__ui-item-desc">Мы решили сделать просто и без лишних элементов.</div>
          </div>
        </div>
        <div class="case__ui-item wow fadeInLeft" data-wow-delay="100ms">
          <div class="case__ui-item-img">
            <img src="/portfolio/pobeda/images/content/item-2.png" alt="">
          </div>
          <div class="case__ui-item-title-wrapper">
            <div class="case__ui-item-title">Карточка продукта</div>
            <div class="case__ui-item-desc">Обычное состояние</div>
          </div>
        </div>
        <div class="case__ui-item wow fadeInLeft" data-wow-delay="150ms">
          <div class="case__ui-item-img">
            <img src="/portfolio/pobeda/images/content/item-3.png" alt="">
          </div>
          <div class="case__ui-item-title-wrapper">
            <div class="case__ui-item-title">Карточка продукта</div>
            <div class="case__ui-item-desc">При наведении</div>
          </div>
        </div>
        <div class="case__ui-item wow fadeInLeft" data-wow-delay="200ms">
          <div class="case__ui-item-img">
            <img src="/portfolio/pobeda/images/content/item-4.png" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="case__adaptive">
  <div class="container">
      <div class="case__header wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
        <div class="case__head-left">
          <div class="case__head-title-wrapper">
            <div class="case__head-title">Адаптивность</div>
          </div>
            <div class="case__head-desc">Сайт адаптируется под все виды устройств, которые есть на рынке.
            </div>
        </div>
      </div>
      <div class="case__adaptives">
        <div class="case__ad-mobile wow fadeInLeft">
          <img src="/portfolio/pobeda/images/content/mobile_adaptive.png" alt="Mobile">
        </div>
        <div class="case__ad-table wow fadeInRight">
          <img src="/portfolio/pobeda/images/content/tablet_adaptive.png" alt="Tablet">
        </div>
        <div class="case__bg-gradient"></div>

      </div>

  </div>
  <div class="case__bg-wheels">
    <img src="/portfolio/pobeda/images/content/coleso.png" alt="wheels">
  </div>
</div>
<div class="case__fonts-section">
  <div class="container">
    <div class="case__header wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
      <div class="case__head-left">
        <div class="case__head-title-wrapper">
          <div class="case__head-title">Типографика & Цвет</div>
        </div>
        <div class="case__head-desc">Мы используем довольно строгую цветовую палитру, а также аккуратная пара шрифтов.
        </div>
      </div>
      <div class="case__head-text">

      </div>
    </div>
    <div class="case__fonts">
      <div class="case__font case__font1 wow fadeIn" data-wow-delay="0">
        <div class="case__font-letter">Aa</div>
        <div class="case__font-title">Open Sans Regular</div>
      </div>
      <div class="case__font case__font2 wow fadeIn" data-wow-delay="50ms">
        <div class="case__font-letter">Aa</div>
        <div class="case__font-title">Open Sans SemiBold</div>
      </div>
      <div class="case__font case__font3 wow fadeIn" data-wow-delay="100ms">
        <div class="case__font-letter">Aa</div>
        <div class="case__font-title">Foros Medium</div>
      </div>
    </div>
    <div class="case__colors">
      <div class="case__color wow fadeIn" data-wow-delay="0">
        <div class="case__color-img">
          <img src="/portfolio/pobeda/images/content/c1.png" alt="">
        </div>
        <div class="case__color-title">
          #e1b25c <br/>
          #b68045
        </div>
      </div>
      <div class="case__color wow fadeIn" data-wow-delay="50ms">
        <div class="case__color-img">
          <img src="/portfolio/pobeda/images/content/c2.png" alt="">
        </div>
        <div class="case__color-title">
          #b8b8b8
        </div>
      </div>
      <div class="case__color wow fadeIn" data-wow-delay="100ms">
        <div class="case__color-img">
          <img src="/portfolio/pobeda/images/content/c3.png" alt="">
        </div>
        <div class="case__color-title">
          #363636
        </div>
      </div>
      <div class="case__color wow fadeIn" data-wow-delay="150ms">
        <div class="case__color-img">
          <img src="/portfolio/pobeda/images/content/c4.png" alt="">
        </div>
        <div class="case__color-title">
          #342c42
        </div>
      </div>
    </div>
  </div>
</div>
<div class="case__layouts">
  <div class="container">
    <div class="case__header wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
      <div class="case__head-left">
        <div class="case__head-title-wrapper">
          <div class="case__head-title">Остальные страницы</div>
        </div>
          <div class="case__head-desc">Было нарисовано более 15 внутренних страниц с различным типам контента
              +&nbsp;все всплывающие окна.
          </div>
      </div>
    </div>
  </div>
    <div class="case__pages-wrapper">
      <div class="case__pages-bg"></div>
      <div class="container">
        <div class="case__pages">
          <div class="gutter-sizer"></div>
          <div class="grid-sizer"></div>
          <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="0ms">
            <div class="case__page-image">
              <div class="case__page-img">
                <img src="/portfolio/pobeda/images/content/page1.png" alt="">
              </div>
            </div>
          </div>
          <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="100ms">
            <div class="case__page-image">
              <div class="case__page-img">
                <img src="/portfolio/pobeda/images/content/page2.png" alt="">
              </div>
            </div>
          </div>
          <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="200ms">
            <div class="case__page-image">
              <div class="case__page-img">
                <img src="/portfolio/pobeda/images/content/page3.png" alt="">
              </div>
            </div>
          </div>
          <div class="case__page case__page--width2 wow fadeInUpBig">
            <div class="case__page-image">
              <div class="case__page-img">
                <img src="/portfolio/pobeda/images/content/page4.png" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="case__bg wow fadeIn" data-wow-offset="400" data-wow-duration="1s">
          <img src="/portfolio/pobeda/images/content/macbook.png" alt="Pobeda">
        </div>
      </div>
    </div>
  </div>
