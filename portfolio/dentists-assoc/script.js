$(document).ready(function(){
    wowJs();
});

$(window).on('load', function(){
    innerMasonry();
});

function wowJs() {

    new WOW().init();
}


function innerMasonry() {
    if ($('.case__page-illu-elems').length > 0) {
        $('.case__page-illu-elems').each(function(){
            $(this).masonry({
                percentPosition: true,
                itemSelector: '.case__page-illu-elem',
                columnWidth: '.grid-sizer'

            });
        });

    }
}