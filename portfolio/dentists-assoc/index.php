<div class="case__bg"></div>

<div class="case__pages">
    <div class="case__page case__page--main">
        <div class="container">
            <div class="case__page-container">

                <div class="case__page-title wow flipInX" data-wow-duration="1s">Главный экран</div>

                <div class="case__page-img wow fadeIn">
                    <img src="/portfolio/dentists-assoc/images/content/main.jpg" alt="Главная страница">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--parents">
        <div class="case__page-picimg wow fadeInLeftBig"></div>
        <div class="container">
            <div class="case__page-elems-bg wow fadeIn" data-wow-duration="2s"></div>
            <div class="case__page-container">

                <div class="case__page-title case__page-title--white wow flipInX" data-wow-duration="1s">Родители и дети</div>

                <div class="case__page-img wow fadeIn">
                    <img src="/portfolio/dentists-assoc/images/content/parents_bg.jpg" alt="Родители и дети">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--medics">
        <div class="case__page-picimg wow fadeInRightBig"></div>
        <div class="container">
            <div class="case__page-elems-medic wow fadeIn" data-wow-duration="2s"></div>
            <div class="case__page-container">

                <div class="case__page-title case__page-title--white wow flipInX" data-wow-duration="1s">Врачи и клиники</div>

                <div class="case__page-img wow fadeIn">
                    <img src="/portfolio/dentists-assoc/images/content/medic.jpg" alt="Врачи и клиники">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--events">
        <div class="case__page-picimg wow fadeInRightBig"></div>
        <div class="container">
            <div class="case__page-picimg-left"></div>
            <div class="case__page-elems-events wow fadeIn" data-wow-duration="2s"></div>
            <div class="case__page-container">

                <div class="case__page-title case__page-title--white wow flipInX" data-wow-duration="1s">События</div>

                <div class="case__page-img wow zoomIn">
                    <img src="/portfolio/dentists-assoc/images/content/events.jpg" alt="События">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--articles">
        <div class="container">
            <div class="case__page-container">
                <div class="case__page-title wow flipInX" data-wow-duration="1s">Статьи</div>
                <div class="case__slider owl-carousel wow fadeIn">
                    <div class="case__page-img">
                        <img src="/portfolio/dentists-assoc/images/content/slide1.jpg">
                    </div>
                    <div class="case__page-img">
                        <img src="/portfolio/dentists-assoc/images/content/slide2.jpg">
                    </div>
                    <div class="case__page-img">
                        <img src="/portfolio/dentists-assoc/images/content/slide3.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--news">
        <div class="case__page-picimg wow fadeInLeftBig"></div>
        <div class="container">
            <div class="case__page-elems-news wow zoomIn" data-wow-duration="2s"></div>
            <div class="case__page-container">

                <div class="case__page-title case__page-title--white wow flipInX" data-wow-duration="1s">Новости</div>

                <div class="case__page-img wow zoomIn">
                    <img src="/portfolio/dentists-assoc/images/content/news.jpg" alt="Новости">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--childhood">
        <div class="container">
            <div class="case__page-elems-childhood wow zoomIn" data-wow-duration="2s"></div>
            <div class="case__page-container">

                <div class="case__page-title wow flipInX" data-wow-duration="1s">Доктора</div>

                <div class="case__page-img wow zoomIn">
                    <img src="/portfolio/dentists-assoc/images/content/childhood.jpg" alt="Доктора">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--illustrations">
        <div class="case__page-bg wow fadeIn" data-wow-duration="2s">
            <img src="/portfolio/dentists-assoc/images/content/illu_fon.png" alt="">
        </div>
        <div class="container">

            <div class="case__page-illu-elems">
                <div class="gutter-sizer"></div>
                <div class="grid-sizer"></div>
                <div class="case__page-illu-elem">
                    <div class="case__page-title">
                        Для данного проекта были профессионально отрисованные следующие иллюстрации
                    </div>
                </div>
                <div class="case__page-illu-elem case__page--illu1 wow fadeInUp" data-wow-delay="100ms">

                    <img src="/portfolio/dentists-assoc/images/content/illu1.png" alt="">

                </div>
                <div class="case__page-illu-elem case__page--illu2 wow fadeInUp" data-wow-delay="150ms">
                    <img src="/portfolio/dentists-assoc/images/content/illu2.png" alt="">
                </div>
                <div class="case__page-illu-elem case__page--illu3 wow fadeInUp" data-wow-delay="150ms">
                    <img src="/portfolio/dentists-assoc/images/content/illu3.png" alt="">
                </div>
                <div class="case__page-illu-elem case__page--illu4 wow fadeInUp" data-wow-delay="150ms">
                    <img src="/portfolio/dentists-assoc/images/content/illu4.png" alt="">
                </div>
                <div class="case__page-illu-elem case__page--illu5 wow fadeInUp" data-wow-delay="150ms">
                    <img src="/portfolio/dentists-assoc/images/content/illu5.png" alt="">
                </div>
                <div class="case__page-illu-elem case__page--illu6 wow fadeInUp" data-wow-delay="150ms">
                    <img src="/portfolio/dentists-assoc/images/content/illu6.png" alt="">
                </div>
                <div class="case__page-illu-elem case__page--illu7 wow fadeInUp" data-wow-delay="150ms">
                    <img src="/portfolio/dentists-assoc/images/content/illu7.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--fonts">
        <div class="case__page--fonts-bg"></div>
        <div class="container">
            <div class="case__fonts">
                <div class="case__fonts-title wow flipInX" data-wow-duration="1s">
                    Шрифты и цветовая палитра
                </div>
                <div class="case__fonts-wrapper">
                    <div class="case__font case__font--marmelad">
                        <div class="case__font-letter wow bounce" data-wow-duration="1s">Aa</div>
                        <div class="case__font-name wow flash" data-wow-duration="1s">Marmelad</div>
                    </div>
                    <div class="case__font case__font--gotham">
                        <div class="case__font-letter wow bounce" data-wow-duration="1s">Aa</div>
                        <div class="case__font-name wow flash" data-wow-duration="1s">Gotham Pro</div>
                    </div>
                </div>
            </div>
            <div class="case__colors row">
                <div class="col-xs-2"><div class="case__color case__color--1 wow bounceInUp" data-wow-delay="100ms"></div></div>
                <div class="col-xs-2"><div class="case__color case__color--2 wow bounceInUp" data-wow-delay="140ms"></div></div>
                <div class="col-xs-2"><div class="case__color case__color--3 wow bounceInUp" data-wow-delay="180ms"></div></div>
                <div class="col-xs-2"><div class="case__color case__color--4 wow bounceInUp" data-wow-delay="220ms"></div></div>
                <div class="col-xs-2"><div class="case__color case__color--5 wow bounceInUp" data-wow-delay="260ms"></div></div>
                <div class="col-xs-2"><div class="case__color case__color--6 wow bounceInUp" data-wow-delay="300ms"></div></div>
            </div>
        </div>
    </div>
    <div class="case__page case__page--404">
      <div class="container">
        <div class="case__page-container">

          <div class="case__page-title case__page-title--white wow flipInX" data-wow-duration="1s">Ошибка 404</div>

          <div class="case__page-img wow zoomIn">
            <img src="/portfolio/dentists-assoc/images/content/404.jpg" alt="Ошибка 404">
          </div>
        </div>
      </div>
      <div class="case__page-mac wow fadeIn" data-wow-duration="1s">
        <img src="/portfolio/dentists-assoc/images/content/mac.png" alt="">
      </div>
    </div>
</div>
