<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="container">
  <div class="case__content case__content--index wow fadeIn" data-wow-offset="200">
    <div class="case__index-title">Главная страница</div>
    <div class="case__index-desc">Краткое описание буквально пару слов</div>
  </div>

  <div class="case__index-bg wow fadeIn" data-wow-duration="1.5s" data-wow-offset="200">
    <img src="/portfolio/credit-car/images/content/main_bg.png" alt="Главная страница">
  </div>
</div>
<div class="case__inner-wraper">
  <div class="container">
    <div class="case__content case__content--inner wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
      <div class="case__index-title">Внутренние страницы</div>
      <div class="case__index-desc">Краткое описание буквально пару слов</div>
    </div>
  </div>
  <div class="case__pages-in">
    <div class="case__page-in case__page1-in wow fadeIn" data-wow-delay="100ms">
      <div class="case__page-image">
        <div class="case__page-img">
          <img src="/portfolio/credit-car/images/content/pages/1.png" alt="Главная">
        </div>
      </div>
    </div>
    <div class="case__page-in case__page2-in wow fadeIn" data-wow-delay="100ms">
      <div class="case__page-image">
        <div class="case__page-img">
          <img src="/portfolio/credit-car/images/content/pages/2.png" alt="Контакты">
        </div>
      </div>
    </div>
    <div class="case__page-in case__page3-in wow fadeIn" data-wow-delay="100ms">
      <div class="case__page-image">
        <div class="case__page-img">
          <img src="/portfolio/credit-car/images/content/pages/3.png" alt="Форма обратной связи">
        </div>
      </div>
    </div>
    <div class="case__page-in case__page4-in wow fadeIn" data-wow-delay="100ms">
      <div class="case__page-image">
        <div class="case__page-img">
          <img src="/portfolio/credit-car/images/content/pages/4.png" alt="Партнеры">
        </div>
      </div>
    </div>
    <div class="case__page-in case__page5-in wow fadeIn" data-wow-delay="100ms">
      <div class="case__page-image">
        <div class="case__page-img">
          <img src="/portfolio/credit-car/images/content/pages/5.png" alt="Faq">
        </div>
      </div>
    </div>
    <div class="case__page-in case__page6-in wow fadeIn" data-wow-delay="100ms">
      <div class="case__page-image">
        <div class="case__page-img">
          <img src="/portfolio/credit-car/images/content/pages/6.png" alt="404">
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="case__content case__content--calc wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
      <div class="case__index-title">Блок калькулятор</div>
      <div class="case__index-desc">Краткое описание буквально пару слов</div>
    </div>
    <div class="case__calc-bg wow fadeIn" data-wow-offset="200">
      <img src="/portfolio/credit-car/images/content/calc1.png" alt="Блок калькулятор">
    </div>
    <div class="case__calc-bg wow fadeIn" data-wow-offset="200">
      <img src="/portfolio/credit-car/images/content/calc2.png" alt="Блок калькулятор">
    </div>
    <div class="case__calc-bg wow fadeIn" data-wow-offset="200">
      <img src="/portfolio/credit-car/images/content/calc3.png" alt="Блок калькулятор">
    </div>
  </div>
</div>
<div class="container">
  <div class="case__main-slider">
    <div class="case__content case__content--slider wow fadeIn" data-wow-offset="200">
      <div class="case__index-title">Слайдер на главной</div>
      <div class="case__index-desc">Я предлагаю сделать гифку, там ведь сочные фото!</div>
    </div>
    <div class="case__index-bg wow fadeIn" data-wow-duration="1.5s" data-wow-offset="200">
      <img src="/portfolio/credit-car/images/content/slider1.png" alt="Слайдер на главной">
    </div>
  </div>
</div>
<div class="case__register">
  <div class="container">
    <div class="case__content case__content--slider wow fadeIn" data-wow-offset="200">
      <div class="case__index-title">Регистрация и вход</div>
      <div class="case__index-desc">Краткое описание буквально пару слов</div>
    </div>
    <div class="case__register-images">
      <div class="case__register-img wow fadeIn" data-wow-offset="200" data-wow-delay="100ms">
        <img src="/portfolio/credit-car/images/content/reg1.png">
      </div>
      <div class="case__register-img wow fadeIn" data-wow-offset="200" data-wow-delay="200ms">
        <img src="/portfolio/credit-car/images/content/reg2.png">
      </div>
      <div class="case__register-img wow fadeIn" data-wow-offset="200" data-wow-delay="100ms">
        <img src="/portfolio/credit-car/images/content/reg3.png">
      </div>
      <div class="case__register-img wow fadeIn" data-wow-offset="200" data-wow-delay="200ms">
        <img src="/portfolio/credit-car/images/content/reg4.png">
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="case_lk">
    <div class="case__content case__content--slider wow fadeIn" data-wow-offset="200">
      <div class="case__index-title">Личный кабинет</div>
      <div class="case__index-desc">Краткое описание буквально пару слов</div>
    </div>
    <div class="case__index-bg wow fadeIn" data-wow-duration="1.5s" data-wow-offset="200">
      <img src="/portfolio/credit-car/images/content/lk.png" alt="Личный кабинет">
    </div>
  </div>
  <div class="case__color-schema">
    <div class="case__content case__content--color wow fadeIn" data-wow-offset="200">
      <div class="case__index-title">Цветовая схема</div>
      <div class="case__index-desc">Краткое описание буквально пару слов</div>
    </div>
    <div class="case__colors">
      <div class="case__color-wrapper wow fadeIn" data-wow-offset="200" data-wow-delay="100ms">
        <div class="case__color case__color1"></div>
        <div class="case__color case__color-title">#CCD963 > #43A46B</div>
      </div>
      <div class="case__color-wrapper wow fadeIn" data-wow-offset="200" data-wow-delay="150ms">
        <div class="case__color case__color2"></div>
        <div class="case__color case__color-title">#9bc53d</div>
      </div>
      <div class="case__color-wrapper wow fadeIn" data-wow-offset="200" data-wow-delay="200ms">
        <div class="case__color case__color3"></div>
        <div class="case__color case__color-title">#97A4B1</div>
      </div>
      <div class="case__color-wrapper wow fadeIn" data-wow-offset="200" data-wow-delay="250ms">
        <div class="case__color case__color4"></div>
        <div class="case__color case__color-title">#000000</div>
      </div>
    </div>
    <div class="case__fonts-wrapper">
      <div class="case__content case__content--color wow fadeIn" data-wow-offset="200">
        <div class="case__index-title">Шрифты</div>
        <div class="case__index-desc">Краткое описание буквально пару слов</div>
      </div>
      <div class="case__fonts">
        <div class="case__font">
          <div class="case__font--letter">Aa</div>
          <div class="case__font--title-wrapper">
            <div class="case__font-title">Open&nbsp;Sans</div>
            <div class="case__font-names">
              <div class="case__font-name">Regular</div>
              <div class="case__font-name">Bold</div>
            </div>
          </div>
        </div>
        <div class="case__font case__font--ubuntu">
          <div class="case__font--letter">Aa</div>
          <div class="case__font--title-wrapper">
            <div class="case__font-title">Ubuntu</div>
            <div class="case__font-names">
              <div class="case__font-name">Regular</div>
              <div class="case__font-name">Bold</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="case__ui">
  <div class="container">
    <div class="case__ui-grid">
      <div class="case__content case__content--ui wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
        <div class="case__index-title">Внутренние страницы</div>
        <div class="case__index-desc">Краткое описание буквально пару слов</div>
        <div class="case__content-ui-text-wrapper">
          <div class="case__content-ui-text">12 колонок</div>
          <div class="case__content-ui-text">1170px</div>
        </div>
      </div>
      <div class="case__pages">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        <div class="case__page case__page--width0 wow fadeIn" data-wow-delay="100ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages0.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUpBig" data-wow-delay="0ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages1.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="50ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages2.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width1 wow fadeInUpBig" data-wow-delay="100ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages3.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="0ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages5.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUpBig" data-wow-delay="50ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages4.png" alt="">
            </div>
          </div>
        </div>

        <div class="case__page case__page--width1 wow fadeInUpBig" data-wow-delay="50ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/pages6.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="case__design">
      <div class="case__content case__content--design wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
        <div class="case__index-title">Адаптивный дизайн</div>
        <div class="case__index-desc">Краткое описание буквально пару слов</div>
      </div>
      <div class="case__pages case__pages--mobile">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="0ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/mpages1.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="100ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/mpages2.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width2 wow fadeInUpBig" data-wow-delay="200ms" data-wow-offset="200">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/credit-car/images/content/mpages3.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="case__mobile-bg wow fadeIn" data-wow-offset="400" data-wow-duration="1s">
        <img src="/portfolio/credit-car/images/content/mobile.png" alt="Creditcar">
      </div>
    </div>
  </div>
</div>
