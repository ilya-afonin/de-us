<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$APPLICATION->SetPageProperty('menuWhite', 'is-white');
?>
<div class="case__container-wrapper">
    <div class="container">
        <div class="case__pages">
            <div class="case__page wow fadeIn" data-wow-offset="250" data-wow-duration="1s">
                <div class="case__page-img">
                    <img src="/portfolio/driver/images/content/img1.png" alt="Экран загрузки">
                </div>
                <div class="case__page-text">
                    <div class="case__page-title">Экран загрузки</div>
                    <div class="case__page-desc">На экране загрузки пользователь получает полезную информацию
                        по использованию мобильного приложения.
                    </div>
                </div>
            </div>
            <div class="case__page case__page--reverse wow fadeIn" data-wow-offset="250" data-wow-duration="1s">
                <div class="case__page-img">
                    <img src="/portfolio/driver/images/content/img2.png" alt="">
                </div>
                <div class="case__page-text">
                    <div class="case__page-title">Главный экран</div>
                    <div class="case__page-desc">
                        На дашборде выводится вся важная информация о пользователе. Скоростной режим, совершенные маневры, пробег, использование телефона и баллы. Каждый день водитель зарабатывает баллы,
                        если выполняет задание дня.
                    </div>
                </div>
            </div>
            <div class="case__page wow fadeIn" data-wow-offset="250" data-wow-duration="1s">
                <div class="case__page-img">
                    <img src="/portfolio/driver/images/content/img3.png" alt="">
                </div>
                <div class="case__page-text">
                    <div class="case__page-title">Призовой экран</div>
                    <div class="case__page-desc">
                        На данном экране пользователь может обменять накопленные баллы на призы, подарки и услуги
                        от партнёров.
                    </div>
                </div>
            </div>
            <div class="case__page case__page--reverse wow fadeIn" data-wow-offset="250" data-wow-duration="1s">
                <div class="case__page-img">
                    <img src="/portfolio/driver/images/content/img4.png" alt="">
                </div>
                <div class="case__page-text">
                    <div class="case__page-title">Экран рейтингов</div>
                    <div class="case__page-desc">
                        На основе данных о вашем вождении приложение позволяет максимально точно рассчитать
                        общий рейтинг среди пользователей.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="case__detail">
        <div class="container">
            <div class="case__page-title-wrapper wow fadeIn" data-wow-offset="100">
                <div class="case__page-title">Детали поездки</div>
                <div class="case__page-desc">Ответственный водитель автоматически оценивает ваши поездки,
                    учитывает маневры, такие как резкие ускорения и торможения, соблюдение скоростного режима и
                    правил дорожного движения, а также использование телефона за рулем.
                </div>
            </div>
        </div>
        <div class="case__detail-images-wrapper">
            <div class="container">
                <div class="case__detail-images">
                    <div class="case__detail-img wow slideInUp" data-wow-delay="50ms">
                        <img src="/portfolio/driver/images/content/det1.png" alt="">
                    </div>
                    <div class="case__detail-img wow slideInUp" data-wow-delay="100ms" data-wow-offset="80">
                        <img src="/portfolio/driver/images/content/det2.png" alt="">
                    </div>
                    <div class="case__detail-img wow slideInUp" data-wow-delay="150ms">
                        <img src="/portfolio/driver/images/content/det3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="case__bg-line case__bg--line1">
        <img src="/portfolio/driver/images/content/bg_line1.svg" alt="">
    </div>
    <div class="case__bg-line case__bg--line2">
        <img src="/portfolio/driver/images/content/bg_line2.svg" alt="">
    </div>

    <div class="container">
        <div class="case__sale">
            <div class="case__page-title-wrapper wow fadeIn" data-wow-offset="100">
                <div class="case__page-title">Акции и уведомления</div>
                <div class="case__page-desc">Эффективное взаимоействие с пользователем мобильного приложения
                    через удобные карточки уведомлений об акциях, событиях.
                </div>
            </div>
            <div class="case__detail-images">
                <div class="case__detail-img wow slideInUp" data-wow-delay="50ms">
                    <img src="/portfolio/driver/images/content/sale1.png" alt="">
                </div>
                <div class="case__detail-img wow slideInUp" data-wow-delay="100ms" data-wow-offset="80">
                    <img src="/portfolio/driver/images/content/sale2.png" alt="">
                </div>
                <div class="case__detail-img wow slideInUp" data-wow-delay="150ms">
                    <img src="/portfolio/driver/images/content/sale3.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="case__icons">
            <div class="case__page-title-wrapper wow fadeIn" data-wow-offset="100">
                <div class="case__page-title">Набор иконок</div>
                <div class="case__page-desc">Позволяют визуально отделять разную информацию,
                    и при этом создавать положительные эмоции.
                </div>
            </div>
            <div class="case__icons-img wow fadeIn" data-wow-offset="200">
                <img src="/portfolio/driver/images/content/icon.png" alt="">
            </div>
        </div>
    </div>
    <div class="case__bg wow fadeIn">
        <img src="/portfolio/driver/images/content/phones.jpg" alt="">
    </div>
</div>
