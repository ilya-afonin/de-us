<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
global $APPLICATION;
$APPLICATION->SetPageProperty('portfolioBlack', 'header--black');
?>

<div class="case__bg"></div>
<div class="container">
  <div class="case__pages">
    <div class="case__page case__page--main">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">01</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Главный экран</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/main.png" alt="Главная страница">
      </div>
    </div>
    <div class="case__page case__page--about">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">02</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">О нас</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/about.png" alt="О нас">
      </div>
    </div>
    <div class="case__page case__page--services">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">03</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Услуги</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/services.png" alt="Услуги">
      </div>
    </div>
    <div class="case__page case__page--projects">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">04</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Проекты</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/projects.png" alt="Проекты">
      </div>
    </div>
    <div class="case__page case__page--clients">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">05</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Клиенты</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/clients.png" alt="Клиенты">
      </div>
    </div>
    <div class="case__page case__page--team">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">06</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Команда</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/team.png" alt="Команда">
      </div>
    </div>
    <div class="case__page case__page--contacts">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">07</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Контакты</div>
      </div>
      <div class="case__page-img wow fadeIn">
        <img src="/portfolio/united/images/content/contacts.png" alt="Контакты">
      </div>
    </div>

    <div class="case__page case__page--fonts">
      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">08</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Сетка, шрифты и цвета</div>
      </div>
      <div class="case__page-img wow fadeIn" style="background-image: url('/portfolio/united/images/content/grid.png')">
        <div class="case__fonts-colors row">
          <div class="case__fonts">
            <div class="case__fonts-title col-sm-2">Шрифт</div>
            <div class="case__fonts-wrapper col-sm-4">
              <div class="case__font-title">Roboto</div>
              <div class="case__font-types">
                <div>Regular</div>
                <div>Medium</div>
                <div>Bold</div>
              </div>
            </div>
          </div>
          <div class="case__colors">
            <div class="case__fonts-title col-sm-2">Цвет</div>
            <div class="case__colors-wrapper col-sm-4">
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#ffffff"/>
                  </svg>
                </div>
                <div class="case__color-name">#ffffff</div>
              </div>
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#e9845f"/>
                  </svg>
                </div>
                <div class="case__color-name">#e9845f</div>
              </div>
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#cb5d87"/>
                  </svg>
                </div>
                <div class="case__color-name">#cb5d87</div>
              </div>
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#8a07e2"/>
                  </svg>
                </div>
                <div class="case__color-name">#8a07e2</div>
              </div>
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#21a5c8"/>
                  </svg>
                </div>
                <div class="case__color-name">#21a5c8</div>
              </div>
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#31b0aa"/>
                  </svg>
                </div>
                <div class="case__color-name">#31b0aa</div>
              </div>
              <div class="case__color">
                <div class="case__color-icon">
                  <svg width="24" height="28" viewBox="0 0 24 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24 16C24 22.6274 18.6274 28 12 28C5.37258 28 0 22.6274 0 16C0 9.37258 12 0 12 0C12 0 24 9.37258 24 16Z" fill="#67bb89"/>
                  </svg>
                </div>
                <div class="case__color-name">#67bb89</div>
              </div>
            </div>
          </div>
        </div>

        <div class="case__grids row">

          <div class="case__grid case__grid-1080 col-sm-12">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-title">Сетка</div>
              <div class="case__dimension-digit">1080px</div>
            </div>
            <div class="case__grid-container"></div>
          </div>

          <div class="case__grid case__grid-712 col-sm-8">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-title"></div>
              <div class="case__dimension-digit">712px</div>
            </div>
            <div class="case__grid-container"></div>
          </div>

          <div class="case__grid case__grid-344 col-sm-4">
            <div class="case__dimension-title-wrapper wow flipInX">
              <div class="case__dimension-title"></div>
              <div class="case__dimension-digit">344px</div>
            </div>
            <div class="case__grid-container"></div>
          </div>

        </div>
      </div>

    </div>

    <div class="case__page case__page--mobile">

      <div class="case__page-title wow fadeIn">
        <div class="case__page-num wow flipInX" data-wow-duration="1s">08</div>
        <div class="case__page-line"></div>
        <div class="case__page-name wow flipInX" data-wow-duration="1s">Адаптив</div>
      </div>
      <div class="case__page-elems">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer col-sm-1"></div>
        <div class="case__elem col-sm-3 wow fadeInUpBig" data-wow-delay="0ms" >
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/united/images/content/mpages1.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__elem case__elem-2 col-sm-3 col-sm-offset-1 wow fadeInUpBig" data-wow-delay="50ms" data-wow-offset="115">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/united/images/content/mpages2.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__elem col-sm-3 col-sm-offset-1 wow fadeInUpBig" data-wow-delay="100ms" >
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/united/images/content/mpages3.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__elem col-sm-3 wow fadeInUpBig" data-wow-delay="0ms" >
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/united/images/content/mpages4.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__elem col-sm-3 col-sm-offset-1 wow fadeInUpBig" data-wow-delay="100ms" data-wow-offset="115">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/united/images/content/mpages6.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__elem col-sm-3 col-sm-offset-1 wow fadeInUpBig" data-wow-delay="50ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/united/images/content/mpages5.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
