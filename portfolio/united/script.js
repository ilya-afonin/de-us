$(document).ready(function(){
    wowJs();
});
$(window).on('load', function(){
    innerMasonry();
});

function wowJs() {

    new WOW().init();
}

function innerMasonry() {
    if ($('.case__page-elems').length > 0) {
        $('.case__page-elems').each(function(){
            $(this).masonry({
                //percentPosition: true,
                itemSelector: '.case__elem',
                columnWidth: '.grid-sizer'

            });
        });

    }
}