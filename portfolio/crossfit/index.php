<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? /*
$APPLICATION->SetPageProperty('portfolioBlack', 'header--black');
*/ ?>
<div class="container">
  <div class="case__content case__content--index wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
    <div class="case__index-title">Главный экран</div>
  </div>
  <div class="case__index-bg wow fadeIn" data-wow-duration="1.5s" data-wow-offset="200">
    <img src="/portfolio/crossfit/images/content/main_bg.png" alt="Главный экран">
  </div>

  <div class="case__content case__content--inner wow fadeIn" data-wow-duration="1s" data-wow-offset="200">
    <div class="case__index-title">+20 внутренних страниц</div>
  </div>
</div>
  <div class="case__pages-wrapper">
    <div class="container">
      <div class="case__pages">
        <div class="case__pages-bg wow fadeInLeft"></div>
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        <div class="case__page case__page--width1 wow fadeInUp" data-wow-delay="100ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-main.png" alt="Главная">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width2 wow fadeInUp" data-wow-delay="150ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-train.png" alt="Тренировки">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="200ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-news.png" alt="Новости">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width2 wow fadeInUp" data-wow-delay="100ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-news-1.png" alt="новости">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="150ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-news-2.png" alt="новости дополнительные">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width1 wow fadeInUp" data-wow-delay="100ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-about.png" alt="О нас">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="200ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/pages-termins.png" alt="Термины">
            </div>
          </div>
        </div>
        <div class="case__content case__content--mobile">
          <div class="case__index-title wow fadeIn" data-wow-offset="200">Мобильная</div>
          <div class="case__mobile-img">
            <img src="/portfolio/crossfit/images/content/men.png" alt="Men">
          </div>
        </div>
      </div>
      <div class="case__pages case__pages--mobile">
        <div class="case__pages-bg"></div>
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="100ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/1.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="200ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/2.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="300ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/3.png" alt="">
            </div>
          </div>
        </div>
        <div class="case__page case__page--width3 wow fadeInUp" data-wow-delay="400ms">
          <div class="case__page-image">
            <div class="case__page-img">
              <img src="/portfolio/crossfit/images/content/4.png" alt="">
            </div>
          </div>
        </div>

      </div>
    </div>


    <div class="case__pages-wrapper-bg"></div>
  </div>

<div class="container">
  <div class="case__elems">
    <div class="case__content case__content--elems wow fadeIn" data-wow-offset="100">
      <div class="case__index-title">элементы сайта</div>
    </div>
    <div class="case__elems-fonts">
      <div class="case__elems-title wow fadeIn">Шрифты</div>
      <div class="case__fonts">
        <div class="case__fonts-col-1">
          <div class="case-font-pfdin wow pulse" data-wow-duration="1s" data-wow-offset="200">PF Din Text Comp Pro
          </div>
          <div class="case-font-h1 wow fadeIn" data-wow-delay="50" data-wow-offset="200">Заголовок <span class="case-font">120px</span>
          </div>
          <div class="case-font-h2 wow fadeIn" data-wow-delay="100" data-wow-offset="200">Заголовок <span class="case-font">70px</span>
          </div>
          <div class="case-font-h3 wow fadeIn" data-wow-delay="150" data-wow-offset="200">Заголовок <span class="case-font">40px</span>
          </div>
        </div>
        <div class="case__fonts-col-2">
          <div class="case-font-gotham wow pulse" data-wow-duration="1s" data-wow-offset="200">Gotham pro</div>
          <div class="case-font-goth2 wow fadeIn" data-wow-delay="50" data-wow-offset="200">Подзаголовок <span
                    class="case-font">22px</span></div>
          <div class="case-font-gotp wow fadeIn" data-wow-delay="150" data-wow-offset="200">Оформление текста <span
                    class="case-font">18px</span></div>
        </div>
      </div>
    </div>
    <div class="case__elems-colors">
      <div class="case__elems-title wow fadeIn">Цветовая палитра</div>
      <div class="case__colors">
        <div class="case__color wow fadeInLeft" data-wow-delay="50">
          <div class="case__color-round case__color-round--red">
            <div class="case__color-round-inner"></div>
          </div>
          <div class="case__color-title">#FF0000</div>
        </div>
        <div class="case__color wow fadeInLeft" data-wow-delay="100">
          <div class="case__color-round case__color-round--black">
            <div class="case__color-round-inner"></div>
          </div>
          <div class="case__color-title">#000000</div>
        </div>
        <div class="case__color wow fadeInLeft" data-wow-delay="150">
          <div class="case__color-round case__color-round--black2">
            <div class="case__color-round-inner"></div>
          </div>
          <div class="case__color-title">#303237</div>
        </div>
        <div class="case__color wow fadeInLeft" data-wow-delay="200">
          <div class="case__color-round case__color-round--gray">
            <div class="case__color-round-inner"></div>
          </div>
          <div class="case__color-title">#F7F8FA</div>
        </div>
      </div>
    </div>
    <div class="case__elems-photos">
      <div class="case__elems-title wow fadeIn">Эффектные фотографии</div>
      <div class="case__photos">
        <div class="case__photo case__photo-1h wow fadeInLeftBig">
          <img src="/portfolio/crossfit/images/content/photo1.png" alt="photo1">
        </div>
        <div class="case__photo case__photo-1h wow fadeInRightBig">
          <img src="/portfolio/crossfit/images/content/photo2.png" alt="photo2">
        </div>
        <div class="case__photo case__photo-2h wow fadeInLeftBig">
          <img src="/portfolio/crossfit/images/content/photo3.png" alt="photo3">
        </div>
        <div class="case__photo case__photo-2h wow fadeInRightBig">
          <img src="/portfolio/crossfit/images/content/photo4.png" alt="photo4">
        </div>
      </div>
    </div>
  </div>
</div>
