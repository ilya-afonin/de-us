$(document).ready(function(){
    wowJs();
});
$(window).on('load', function(){
    innerMasonry();
});

function wowJs() {

    new WOW().init();
}

function innerMasonry() {
    if ($('.case__pages').length > 0) {
        $('.case__pages').each(function(){
            $(this).masonry({
                percentPosition: true,
                itemSelector: '.case__page',
                columnWidth: '.grid-sizer'

            });
        });

    }
}