$(document).ready(function(){
    wowJs();
});
$(window).on('load', function(){
    innerMasonry();
    initScrollMagic();
});

function wowJs() {

    new WOW().init();
}

function innerMasonry() {
    if ($('.case__page-elems').length > 0) {
        $('.case__page-elems').each(function(){
            $(this).masonry({
                //percentPosition: true,
                itemSelector: '.case__elem',
                columnWidth: '.grid-sizer'

            });
        });

    }
}

function initScrollMagic() {

    // init controller
    var controller = new ScrollMagic.Controller({refreshInterval: 20});

    new ScrollMagic.Scene({
        triggerElement: $('.section-net-img'),
        triggerHook: .8,
        offset: 0//o.isCurrentPage("home") ? $(this).height() / 2 - 175 : 0,
    }).setClassToggle($('.section-net-img'), "visible").addTo(controller)
}