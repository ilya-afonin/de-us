<?
define('PAGE','about');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?>
    <div class="about">
        <section class="s-section s-section--about">
            <div class="container">
                <aside class="sideblock">
                  <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_text.php", Array(), Array("MODE" => "php", "NAME" => 'основной текст')); ?>
                </aside>
                <div class="content-wrapper">
                    <div class="s-content__text">
                        <p class="s__describe">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_describe.php", Array(), Array("MODE" => "php", "NAME" => 'описание')); ?>
                        </p>
                      <?$APPLICATION->IncludeComponent(
                          "bitrix:news.list",
                          "about_digits",
                          array(
                              "ACTIVE_DATE_FORMAT" => "d.m.Y",
                              "ADD_SECTIONS_CHAIN" => "N",
                              "AJAX_MODE" => "N",
                              "AJAX_OPTION_ADDITIONAL" => "",
                              "AJAX_OPTION_HISTORY" => "N",
                              "AJAX_OPTION_JUMP" => "N",
                              "AJAX_OPTION_STYLE" => "Y",
                              "CACHE_FILTER" => "N",
                              "CACHE_GROUPS" => "Y",
                              "CACHE_TIME" => "36000000",
                              "CACHE_TYPE" => "A",
                              "CHECK_DATES" => "Y",
                              "DETAIL_URL" => "",
                              "DISPLAY_BOTTOM_PAGER" => "Y",
                              "DISPLAY_DATE" => "Y",
                              "DISPLAY_NAME" => "Y",
                              "DISPLAY_PICTURE" => "Y",
                              "DISPLAY_PREVIEW_TEXT" => "Y",
                              "DISPLAY_TOP_PAGER" => "N",
                              "FIELD_CODE" => array(
                                  0 => "",
                                  1 => "",
                              ),
                              "FILTER_NAME" => "",
                              "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                              "IBLOCK_ID" => "8",
                              "IBLOCK_TYPE" => "content",
                              "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                              "INCLUDE_SUBSECTIONS" => "Y",
                              "MESSAGE_404" => "",
                              "NEWS_COUNT" => "10",
                              "PAGER_BASE_LINK_ENABLE" => "N",
                              "PAGER_DESC_NUMBERING" => "N",
                              "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                              "PAGER_SHOW_ALL" => "N",
                              "PAGER_SHOW_ALWAYS" => "N",
                              "PAGER_TEMPLATE" => ".default",
                              "PAGER_TITLE" => "Новости",
                              "PARENT_SECTION" => "",
                              "PARENT_SECTION_CODE" => "",
                              "PREVIEW_TRUNCATE_LEN" => "",
                              "PROPERTY_CODE" => array(
                                  0 => "DIGIT",
                                  1 => "",
                              ),
                              "SET_BROWSER_TITLE" => "N",
                              "SET_LAST_MODIFIED" => "N",
                              "SET_META_DESCRIPTION" => "N",
                              "SET_META_KEYWORDS" => "N",
                              "SET_STATUS_404" => "N",
                              "SET_TITLE" => "N",
                              "SHOW_404" => "N",
                              "SORT_BY1" => "SORT",
                              "SORT_BY2" => "ACTIVE_FROM",
                              "SORT_ORDER1" => "ASC",
                              "SORT_ORDER2" => "DESC",
                              "STRICT_SECTION_CHECK" => "N",
                              "COMPONENT_TEMPLATE" => "about_digits"
                          ),
                          false
                      );?>
                    </div>
                </div>
            </div>
        </section>
        <section class="s-section s-section--honors">
            <div class="container">
                <aside class="sideblock">
                    <div class="s-content__text">
                        <div class="s__caption">
                            <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_honor_t.php", Array(), Array("MODE" => "php", "NAME" => 'название')); ?>
                        </div>
                        <p class="s__describe">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_honor_desc.php", Array(), Array("MODE" => "php", "NAME" => 'описание')); ?>
                        </p>
                        <div class="s-section__nav s-arrows"></div>
                    </div>
                </aside>
                <div class="content-wrapper">
                    <div class="s-content__text">
                      <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"about_honors",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "COLOR_BG",
			1 => "COLOR",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "about_honors"
	),
	false
);?>
                    </div>
                </div>
            </div>
        </section>
        <section class="s-section s-section--services" id="services">
            <div class="container">
                <aside class="sideblock">
                    <div class="s-content__text">
                        <div class="s__caption">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_services_t.php", Array(), Array("MODE" => "php", "NAME" => 'название')); ?>
                        </div>
                        <p class="s__describe"><?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_services_descr.php", Array(), Array("MODE" => "php", "NAME" => 'описание')); ?></p>
                    </div>
                </aside>
                <div class="content-wrapper">
                  <?$APPLICATION->IncludeComponent(
                      "bitrix:news.list",
                      "about_services",
                      array(
                          "ACTIVE_DATE_FORMAT" => "d.m.Y",
                          "ADD_SECTIONS_CHAIN" => "N",
                          "AJAX_MODE" => "N",
                          "AJAX_OPTION_ADDITIONAL" => "",
                          "AJAX_OPTION_HISTORY" => "N",
                          "AJAX_OPTION_JUMP" => "N",
                          "AJAX_OPTION_STYLE" => "Y",
                          "CACHE_FILTER" => "N",
                          "CACHE_GROUPS" => "Y",
                          "CACHE_TIME" => "36000000",
                          "CACHE_TYPE" => "A",
                          "CHECK_DATES" => "Y",
                          "DETAIL_URL" => "",
                          "DISPLAY_BOTTOM_PAGER" => "Y",
                          "DISPLAY_DATE" => "Y",
                          "DISPLAY_NAME" => "Y",
                          "DISPLAY_PICTURE" => "Y",
                          "DISPLAY_PREVIEW_TEXT" => "Y",
                          "DISPLAY_TOP_PAGER" => "N",
                          "FIELD_CODE" => array(
                              0 => "",
                              1 => "",
                          ),
                          "FILTER_NAME" => "",
                          "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                          "IBLOCK_ID" => "3",
                          "IBLOCK_TYPE" => "content",
                          "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                          "INCLUDE_SUBSECTIONS" => "Y",
                          "MESSAGE_404" => "",
                          "NEWS_COUNT" => "20",
                          "PAGER_BASE_LINK_ENABLE" => "N",
                          "PAGER_DESC_NUMBERING" => "N",
                          "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                          "PAGER_SHOW_ALL" => "N",
                          "PAGER_SHOW_ALWAYS" => "N",
                          "PAGER_TEMPLATE" => ".default",
                          "PAGER_TITLE" => "Новости",
                          "PARENT_SECTION" => "",
                          "PARENT_SECTION_CODE" => "",
                          "PREVIEW_TRUNCATE_LEN" => "",
                          "PROPERTY_CODE" => array(
                              0 => "",
                              1 => "",
                          ),
                          "SET_BROWSER_TITLE" => "N",
                          "SET_LAST_MODIFIED" => "N",
                          "SET_META_DESCRIPTION" => "N",
                          "SET_META_KEYWORDS" => "N",
                          "SET_STATUS_404" => "N",
                          "SET_TITLE" => "N",
                          "SHOW_404" => "N",
                          "SORT_BY1" => "SORT",
                          "SORT_BY2" => "ACTIVE_FROM",
                          "SORT_ORDER1" => "ASC",
                          "SORT_ORDER2" => "DESC",
                          "STRICT_SECTION_CHECK" => "N",
                          "COMPONENT_TEMPLATE" => "about_services"
                      ),
                      false
                  );?>
                </div>
            </div>
        </section>
        <section class="s-section s-section--clients">
            <div class="container">
                <aside class="sideblock">
                    <div class="s-content__text">
                        <div class="s__caption">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_clients_t.php", Array(), Array("MODE" => "php", "NAME" => 'название')); ?>
                        </div>
                        <p class="s__describe">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_clients_descr.php", Array(), Array("MODE" => "php", "NAME" => 'описание')); ?>
                        </p>
                    </div>
                </aside>
                <div class="content-wrapper">
                    <div class="s-content__text">
                      <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"about_clients",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "about_clients"
	),
	false
);?>
                    </div>
                </div>
            </div>
        </section>
        <section class="s-section s-section--form" id="s-callback">
            <div class="container">
                <aside class="sideblock">
                    <div class="s-content__text">
                        <div class="s__caption">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_form_t.php", Array(), Array("MODE" => "php", "NAME" => 'название')); ?>
                        </div>
                        <p class="s__describe">
                          <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/about_form_descr.php", Array(), Array("MODE" => "php", "NAME" => 'описание')); ?>
                        </p>
                    </div>
                </aside>
                <div class="content-wrapper">
                    <div class="s-content__text">
                      <?php $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/form.php", Array(), Array("MODE" => "php", "NAME" => 'форму')); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>